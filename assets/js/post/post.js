import '../../styles/post/post.scss';

import $ from 'jquery';
import 'select2';
import 'select2/dist/css/select2.css';
import 'select2-bootstrap-5-theme/dist/select2-bootstrap-5-theme.css';
import '../_post_reactions';
import {matchCustom} from '../_tags_synonyms';

$(document).ready(function () {
    $('#post_subject').select2({
        theme: "bootstrap-5"
    });

    $('#post_tags').select2({
        theme: "bootstrap-5",
        matcher: matchCustom
    });
});