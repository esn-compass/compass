import '../../styles/profile/profile.scss';

import $ from 'jquery';
import 'select2';
import 'select2/dist/css/select2.css';
import 'select2-bootstrap-5-theme/dist/select2-bootstrap-5-theme.css';
import '../_post_reactions';

$(document).ready(function() {
    $('#profile_form_homeCountries').select2();
    $('#profile_form_languages').select2();
});

$(document).ready(function() {
    const $wrapper = $('.snsInfos');
    $wrapper.on('click', '.delete-item-link', function(e) {
        e.preventDefault();
        $(this).closest('.sns-item')
            .remove();
    });
    $wrapper.on('click', '.add-item-link', function(e) {
        e.preventDefault();
        // Get the data-prototype explained earlier
        let prototype = $wrapper.data('prototype');
        // get the new index
        let index = $wrapper.data('index');
        // Replace '__name__' in the prototype's HTML to
        // instead be a number based on how many items we have
        let newForm = prototype.replace(/__name__/g, index);
        // increase the index with one for the next item
        $wrapper.data('index', index + 1);
        // Display the form in the page before the "new" link
        $(this).before(newForm);
    });
});

$(document).ready(function() {
    $('#user_education_institution').select2({
        theme: "bootstrap-5"
    });
    $('#country_selection').select2({
        theme: "bootstrap-5"
    });
});