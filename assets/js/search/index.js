import '../../styles/search/index.scss';
import $ from 'jquery';
import 'select2';
import 'select2/dist/css/select2.css';
import 'select2-bootstrap-5-theme/dist/select2-bootstrap-5-theme.css';
import '../_post_reactions';
import {matchCustom} from '../_tags_synonyms';

$(document).ready(function() {
    $('#tags').select2({
        theme: "bootstrap-5",
        placeholder: "#Tags",
        matcher: matchCustom
    });
    $('#place').select2({
        theme: "bootstrap-5",
        placeholder: "School | City | Country",
        allowClear: true
    });

    $("#filters").submit(function() {
        $(this).find(":input").filter(function(){return !this.value;}).attr("disabled", "disabled");
    });
    // Un-disable form fields when page loads, in case they click back after submission or client side validation
    $("#filters").find(":input").prop("disabled", false);
});