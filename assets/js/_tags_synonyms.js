import $ from "jquery";

export function matchCustom(params, data) {
    // If there are no search terms, return all of the data
    if ($.trim(params.term) === '') {
        return data;
    }

    // Do not display the item if there is no 'text' property
    if (typeof data.text === 'undefined') {
        return null;
    }

    // `params.term` should be the term that is used for searching
    // `data.text` is the text that is displayed for the data object
    if (data.text.indexOf(params.term) > -1) {
        return data;
    }

    // check if some synonyms matches the term
    if ($(data.element).data('synonyms').toString().indexOf(params.term) > -1) {
        let modifiedData = $.extend({}, data, true);

        let matchedSynonyms = $(data.element).data('synonyms').toString().split(';').filter(
            syn => syn.indexOf(params.term) > -1
        );

        modifiedData.text += ' (' + matchedSynonyms.toString() + ')';
        return modifiedData;
    }

    return null;
}