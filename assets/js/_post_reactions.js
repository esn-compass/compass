import $ from 'jquery';

$(document).ready(function () {

    function onClickBtnReaction(event) {
        event.preventDefault();

        let clickedBtn = this;
        $.post(this.dataset.target, function (data) {
            if (data.response) {
                $(clickedBtn).closest('.reactions').html(data.response);
            }
            $('div.reaction-btn-group a').off('click').click(onClickBtnReaction);
        }).fail(function (data) {
            console.log('fail', data);
        });
    }

    $('div.reaction-btn-group a').click(onClickBtnReaction);
});
