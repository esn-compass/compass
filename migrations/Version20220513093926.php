<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220513093926 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE city (id INT AUTO_INCREMENT NOT NULL, big_city_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(50) DEFAULT NULL, country VARCHAR(2) NOT NULL, synonyms LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', INDEX IDX_2D5B0234EC090179 (big_city_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE institution (id INT AUTO_INCREMENT NOT NULL, city_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(100) DEFAULT NULL, INDEX IDX_3A9F98E58BAC62AF (city_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE post (id INT AUTO_INCREMENT NOT NULL, author_id INT NOT NULL, institution_id INT DEFAULT NULL, city_id INT DEFAULT NULL, moderated_by_id INT DEFAULT NULL, type VARCHAR(50) NOT NULL, title VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, country VARCHAR(2) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME DEFAULT NULL, moderation_status VARCHAR(50) NOT NULL, moderated_at DATETIME DEFAULT NULL, INDEX IDX_5A8A6C8DF675F31B (author_id), INDEX IDX_5A8A6C8D10405986 (institution_id), INDEX IDX_5A8A6C8D8BAC62AF (city_id), INDEX IDX_5A8A6C8D8EDA19B0 (moderated_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE post_tag (post_id INT NOT NULL, tag_id INT NOT NULL, INDEX IDX_5ACE3AF04B89032C (post_id), INDEX IDX_5ACE3AF0BAD26311 (tag_id), PRIMARY KEY(post_id, tag_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE post_idea (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(20) NOT NULL, title VARCHAR(255) NOT NULL, question VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE post_idea_tag (post_idea_id INT NOT NULL, tag_id INT NOT NULL, INDEX IDX_F95F34DF6687584E (post_idea_id), INDEX IDX_F95F34DFBAD26311 (tag_id), PRIMARY KEY(post_idea_id, tag_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sns_info (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, type VARCHAR(50) NOT NULL, content VARCHAR(100) NOT NULL, INDEX IDX_414CB79CA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tag (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, name VARCHAR(50) NOT NULL, synonyms LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', INDEX IDX_389B783727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, firstname VARCHAR(50) NOT NULL, lastname VARCHAR(50) DEFAULT NULL, username VARCHAR(50) DEFAULT NULL, gender SMALLINT NOT NULL, birthdate DATE DEFAULT NULL, home_countries LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', languages LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', current_situation VARCHAR(255) DEFAULT NULL, sns_visibility VARCHAR(50) NOT NULL, status VARCHAR(50) NOT NULL, last_login DATETIME DEFAULT NULL, is_verified TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_education (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, institution_id INT DEFAULT NULL, country VARCHAR(10) DEFAULT NULL, location VARCHAR(255) DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, start_date VARCHAR(255) NOT NULL, end_date VARCHAR(255) NOT NULL, local TINYINT(1) NOT NULL, INDEX IDX_DBEAD336A76ED395 (user_id), INDEX IDX_DBEAD33610405986 (institution_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE city ADD CONSTRAINT FK_2D5B0234EC090179 FOREIGN KEY (big_city_id) REFERENCES city (id)');
        $this->addSql('ALTER TABLE institution ADD CONSTRAINT FK_3A9F98E58BAC62AF FOREIGN KEY (city_id) REFERENCES city (id)');
        $this->addSql('ALTER TABLE post ADD CONSTRAINT FK_5A8A6C8DF675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE post ADD CONSTRAINT FK_5A8A6C8D10405986 FOREIGN KEY (institution_id) REFERENCES institution (id)');
        $this->addSql('ALTER TABLE post ADD CONSTRAINT FK_5A8A6C8D8BAC62AF FOREIGN KEY (city_id) REFERENCES city (id)');
        $this->addSql('ALTER TABLE post ADD CONSTRAINT FK_5A8A6C8D8EDA19B0 FOREIGN KEY (moderated_by_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE post_tag ADD CONSTRAINT FK_5ACE3AF04B89032C FOREIGN KEY (post_id) REFERENCES post (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE post_tag ADD CONSTRAINT FK_5ACE3AF0BAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE post_idea_tag ADD CONSTRAINT FK_F95F34DF6687584E FOREIGN KEY (post_idea_id) REFERENCES post_idea (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE post_idea_tag ADD CONSTRAINT FK_F95F34DFBAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sns_info ADD CONSTRAINT FK_414CB79CA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE tag ADD CONSTRAINT FK_389B783727ACA70 FOREIGN KEY (parent_id) REFERENCES tag (id)');
        $this->addSql('ALTER TABLE user_education ADD CONSTRAINT FK_DBEAD336A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_education ADD CONSTRAINT FK_DBEAD33610405986 FOREIGN KEY (institution_id) REFERENCES institution (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE city DROP FOREIGN KEY FK_2D5B0234EC090179');
        $this->addSql('ALTER TABLE institution DROP FOREIGN KEY FK_3A9F98E58BAC62AF');
        $this->addSql('ALTER TABLE post DROP FOREIGN KEY FK_5A8A6C8D8BAC62AF');
        $this->addSql('ALTER TABLE post DROP FOREIGN KEY FK_5A8A6C8D10405986');
        $this->addSql('ALTER TABLE user_education DROP FOREIGN KEY FK_DBEAD33610405986');
        $this->addSql('ALTER TABLE post_tag DROP FOREIGN KEY FK_5ACE3AF04B89032C');
        $this->addSql('ALTER TABLE post_idea_tag DROP FOREIGN KEY FK_F95F34DF6687584E');
        $this->addSql('ALTER TABLE post_tag DROP FOREIGN KEY FK_5ACE3AF0BAD26311');
        $this->addSql('ALTER TABLE post_idea_tag DROP FOREIGN KEY FK_F95F34DFBAD26311');
        $this->addSql('ALTER TABLE tag DROP FOREIGN KEY FK_389B783727ACA70');
        $this->addSql('ALTER TABLE post DROP FOREIGN KEY FK_5A8A6C8DF675F31B');
        $this->addSql('ALTER TABLE post DROP FOREIGN KEY FK_5A8A6C8D8EDA19B0');
        $this->addSql('ALTER TABLE sns_info DROP FOREIGN KEY FK_414CB79CA76ED395');
        $this->addSql('ALTER TABLE user_education DROP FOREIGN KEY FK_DBEAD336A76ED395');
        $this->addSql('DROP TABLE city');
        $this->addSql('DROP TABLE institution');
        $this->addSql('DROP TABLE post');
        $this->addSql('DROP TABLE post_tag');
        $this->addSql('DROP TABLE post_idea');
        $this->addSql('DROP TABLE post_idea_tag');
        $this->addSql('DROP TABLE sns_info');
        $this->addSql('DROP TABLE tag');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_education');
    }
}
