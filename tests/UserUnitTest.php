<?php

namespace App\Tests;

use App\Entity\User;
use App\Enum\SituationEnum;
use App\Enum\SnsVisibilityEnum;
use App\Enum\UserStatusEnum;
use PHPUnit\Framework\TestCase;

class UserUnitTest extends TestCase
{
    public function testGettersAndDefaultValues(): void
    {
        $user = new User();

        $user->setEmail('test@example.com');
        $user->setPassword('testpass');
        $user->setFirstname('John');
        $user->setLastname('Doe');
        $user->setUsername('jdoe42');
        $user->setGender(User::GENDER_MALE);
        $user->setBirthdate(new \DateTime('1990-01-01'));
        $user->setHomeCountries(['fr', 'es']);
        $user->setCurrentSituation(SituationEnum::LOCAL);
        $user->setIsVerified(1);

        $this->assertEquals('test@example.com', $user->getEmail());
        $this->assertEquals('testpass', $user->getPassword());
        $this->assertEquals('John', $user->getFirstname());
        $this->assertEquals('Doe', $user->getLastname());
        $this->assertEquals('jdoe42', $user->getUsername());
        $this->assertEquals(User::GENDER_MALE, $user->getGender());
        $this->assertEquals('1990-01-01', $user->getBirthdate()->format('Y-m-d'));
        $this->assertEquals(['fr', 'es'], $user->getHomeCountries());
        $this->assertEquals(SituationEnum::LOCAL, $user->getCurrentSituation());
        $this->assertEquals(SnsVisibilityEnum::PUBLIC, $user->getSnsVisibility());
        $this->assertEquals(UserStatusEnum::SAVED, $user->getStatus());
        $this->assertNull($user->getLastLogin());
    }
}
