<?php

namespace Deployer;

require 'recipe/symfony4.php';
require 'recipe/yarn.php';

set('application', 'compass');
set('repository', 'https://gitlab.com/esn-compass/compass.git');

set('git_tty', false);
set('bin/php', '/usr/bin/php7.4');

set('http_user', 'www-data');
set('keep_releases', 3);

// Shared files/dirs between deploys 
add('shared_files', []);
add('shared_dirs', ['public/documents']);

set('clear_paths', [
    '/tests',
    '.docker',
    '.env.test',
    '.git',
    '.gitignore',
    '.gitlab-ci.yml',
    'deploy.php',
    'docker-compose.yml',
    'docker-compose.override.yml.dist',
]);

set('bin/composer', function () {
    run('cd {{release_path}} && curl -sS https://getcomposer.org/installer | {{bin/php}}');
    $composer = '{{release_path}}/composer.phar';

    return '{{bin/php}} ' . $composer;
});

// Hosts

host('prod', 'staging', 'demo')
    ->configFile('deploy_config')
    ->addSshOption('StrictHostKeyChecking', 'no')
    ->addSshOption('UserKnownHostsFile', '/dev/null')
    ->multiplexing(false)
    ->forwardAgent(true);

host('prod')
    ->stage('prod')
    ->set('host', 'prod')
    ->set('symfony_env', 'prod')
    ->set('branch', 'dev')
    ->set('deploy_path', '/srv/www/compass-youthmobility.eu/prod');

host('staging')
    ->stage('staging')
    ->set('host', 'staging')
    ->set('symfony_env', 'dev')
    ->set('branch', 'dev')
    ->set('deploy_path', '/srv/www/compass-youthmobility.eu/staging')
    ->set('composer_options', '{{composer_action}} --verbose --prefer-dist --no-progress --no-interaction --optimize-autoloader');

host('demo')
    ->stage('demo')
    ->set('host', 'demo')
    ->set('symfony_env', 'prod')
    ->set('branch', 'dev')
    ->set('deploy_path', '/srv/www/compass-youthmobility.eu/demo')
    ->set('composer_options', '{{composer_action}} --verbose --prefer-dist --no-progress --no-interaction --no-dev --optimize-autoloader');

// Tasks

desc('Compile .env files for production');
task('deploy:dump-env', function () {
    run('cd {{release_path}} && {{bin/composer}} dump-env {{symfony_env}}');
});

desc('Build CSS/JS files');
task('deploy:build_local_assets', function () {
    run('cd {{release_path}} && {{bin/yarn}} run {{symfony_env}}');
});

desc('Sync metadata storage for doctrine migration');
task('database:sync-meta', function () {
    run(sprintf('{{bin/php}} {{bin/console}} doctrine:migrations:sync-metadata-storage {{console_options}}'));
});

desc('Drop database');
task('database:drop', function () {
    run('{{bin/php}} {{bin/console}} doctrine:database:drop --force --if-exists');
    run('{{bin/php}} {{bin/console}} doctrine:database:create --if-not-exists');
})->onHosts('demo');

desc('Load fixtures into database');
task('database:load-fixtures', function () {
    run('{{bin/php}} {{bin/console}} doctrine:fixtures:load --append');
})->onHosts('demo');


after('deploy:update_code', 'deploy:clear_paths');
after('deploy:shared', 'yarn:install');
after('deploy:vendors', 'deploy:dump-env');
after('deploy:vendors', 'deploy:build_local_assets');
before('database:sync-meta', 'database:drop');
before('database:migrate', 'database:sync-meta');
before('deploy:symlink', 'database:migrate');
after('database:migrate', 'database:load-fixtures');
after('deploy:failed', 'deploy:unlock');
