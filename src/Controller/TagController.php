<?php

namespace App\Controller;

use App\Entity\City;
use App\Entity\Institution;
use App\Repository\CityRepository;
use App\Repository\InstitutionRepository;
use App\Repository\PostRepository;
use App\Repository\TagRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class TagController extends AbstractController
{
    /**
     * @Route("/tags/{tags}", name="tags_list")
     */
    public function listPostsByTags(PostRepository $postRepo, string $tags = null): Response
    {
        $tags = explode('+', $tags);
        $posts = $postRepo->findAcceptedByTagsNames($tags);

        return $this->render('tags/posts.html.twig', [
            'tags' => $tags,
            'posts' => $posts,
        ]);
    }

    /**
     * @Route("/institution/{slug}", name="institution_list")
     */
    public function listPostsByInstitution(PostRepository $postRepo, Institution $institution): Response
    {
        return $this->render('tags/posts.html.twig', [
            'institution' => $institution,
            'posts' => $postRepo->searchFromFilters(['place' => $institution]),
        ]);
    }

    /**
     * @Route("/city/{slug}", name="city_list")
     */
    public function listPostsByCity(PostRepository $postRepo, City $city): Response
    {
        return $this->render('tags/posts.html.twig', [
            'city' => $city,
            'posts' => $postRepo->searchFromFilters(['place' => $city]),
        ]);
    }

    /**
     * @Route("/country/{country}", name="country_list")
     */
    public function listPostsByCountry(PostRepository $postRepo, string $country): Response
    {
        return $this->render('tags/posts.html.twig', [
            'country' => $country,
            'posts' => $postRepo->searchFromFilters(['place' => $country]),
        ]);
    }
}
