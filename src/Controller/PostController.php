<?php

namespace App\Controller;

use App\Entity\City;
use App\Entity\Comment;
use App\Entity\Institution;
use App\Entity\Post;
use App\Entity\PostIdea;
use App\Entity\User;
use App\Entity\UserEducation;
use App\Enum\ModerationStatusEnum;
use App\Enum\OpenCountryEnum;
use App\Enum\PostIdeaTypeEnum;
use App\Form\CommentType;
use App\Form\PostType;
use App\Repository\CityRepository;
use App\Repository\InstitutionRepository;
use App\Repository\PostIdeaRepository;
use App\Repository\PostReactionRepository;
use App\Repository\TagRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Intl\Countries;
use Symfony\Component\Routing\Annotation\Route;

class PostController extends AbstractController
{
    /**
     * @IsGranted("ROLE_USER")
     * @Route("/posts/new", name="post_new")
     * @Route("/posts/new/tags/{tags}", name="post_new_from_tag")
     * @Route("/posts/new/idea/{id}", name="post_new_from_idea")
     */
    public function new(
        Request $request,
        EntityManagerInterface $em,
        TagRepository $tagRepository,
        InstitutionRepository $institutionRepository,
        CityRepository $cityRepository,
        PostIdea $idea = null,
        string $tags = null
    ): Response {
        /** @var User $user */
        $user = $this->getUser();

        if (false === $user->isVerified()) {
            return $this->redirectToRoute('profile');
        }

        $post = new Post();
        $post->setAuthor($user);

        if (true === $post->getAuthor()->isSkipModeration()) {
            $post->setModerationStatus(ModerationStatusEnum::ACCEPTED);
        }

        if (false === is_null($idea)) {
            $name = '';

            if ($idea->getType() === PostIdeaTypeEnum::INSTITUTION && $request->query->has('institution')) {
                $institution = $institutionRepository->find($request->query->get('institution'));
                $subject = 'inst-' . $institution->getId();
                $name = $institution->getName();
            } elseif ($idea->getType() === PostIdeaTypeEnum::CITY && $request->query->has('city')) {
                $city = $cityRepository->find($request->query->get('city'));
                $subject = 'city-' . $city->getId();
                $name = $city->getName();
            } elseif ($idea->getType() === PostIdeaTypeEnum::COUNTRY && $request->query->has('country')) {
                $subject = $request->query->get('country');
                $name = Countries::getName($request->query->get('country'));
            }

            $title = str_replace('%name%', $name, $idea->getTitle());
            $post->setTitle($title);
            foreach ($idea->getTags() as $tag) {
                $post->addTag($tag);
            }
        }

        if (false === empty($tags)) {
            $tags = explode('+', $tags);
            foreach ($tags as $tag) {
                $post->addTag($tagRepository->findOneBy(['name' => $tag]));
            }
        }

        $form = $this->createForm(PostType::class, $post);
        if (isset($subject)) {
            $form->get('subject')->setData($subject);
        }

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('success', 'Your post has been submitted and will be checked by moderators'
                . ' soon!');
            $em->persist($post);
            $em->flush();
            return $this->redirectToRoute('profile');
        }

        return $this->render('post/new.html.twig', [
            'form' => $form->createView(),
            'idea' => $idea,
        ]);
    }

    /**
     * @Route("/posts/{id}", name="post_show", requirements={"id"="\d+"})
     */
    public function show(Post $post, Request $request, EntityManagerInterface $em): Response
    {
        if ($this->isGranted('ROLE_USER')) {
            $comment = new Comment();
            $comment->setAuthor($this->getUser());
            $comment->setPost($post);
            $commentForm = $this->createForm(CommentType::class, $comment);

            if (true === $comment->getAuthor()->isSkipModeration()) {
                $comment->setModerationStatus(ModerationStatusEnum::ACCEPTED);
            }

            $commentForm->handleRequest($request);
            if ($commentForm->isSubmitted() && $commentForm->isValid()) {
                $this->addFlash('success', 'Your comment has been submitted and will be checked by'
                    . ' moderators soon!');
                $em->persist($comment);
                $em->flush();
                $em->refresh($post);
            }
        }

        $displayedComments = [];
        foreach ($post->getComments() as $comment) {
            if ($comment->isPublic()
                || $this->isGranted('ROLE_ADMIN')
                || $comment->getAuthor() === $this->getUser()) {
                $displayedComments[] = $comment;
            }
        }

        return $this->render('post/show.html.twig', [
            'post' => $post,
            'postComments' => $displayedComments,
            'commentForm' => isset($commentForm) ? $commentForm->createView() : null,
            'topic' => $post->getTopic() instanceof Institution ? [
                'institution' => $post->getInstitution(),
                'city' => $post->getInstitution()->getCity(),
                'country' => $post->getInstitution()->getCity()->getCountry(),
            ] : ($post->getTopic() instanceof City ? [
                'city' => $post->getCity(),
                'country' => $post->getCity()->getCountry(),
            ] : [
                'country' => $post->getCountry()
            ]),
        ]);
    }

    /**
     * @Route("/posts/{id}/delete", name="post_delete", methods="POST")
     */
    public function delete(Post $post, Request $request, EntityManagerInterface $em): Response
    {
        if ($this->isCsrfTokenValid('delete' . $post->getId(), $request->get('_token'))
            && $this->getUser() === $post->getAuthor()) {
            $em->remove($post);
            $em->flush();
            $this->addFlash('success', 'Your post has been successfully deleted');
        }
        return $this->redirectToRoute('profile');
    }

    /**
     * @Route("/comments/{id}/delete", name="comment_delete", methods="POST")
     */
    public function deleteComment(Comment $comment, Request $request, EntityManagerInterface $em): Response
    {
        if ($this->isCsrfTokenValid('delete' . $comment->getId(), $request->get('_token'))
            && $this->getUser() === $comment->getAuthor()) {
            $em->remove($comment);
            $em->flush();
            $this->addFlash('success', 'Your comment has been successfully deleted');
        }
        return $this->redirectToRoute('post_show', ['id' => $comment->getPost()->getId()]);
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/posts/ideas", name="post_ideas")
     */
    public function ideas(PostIdeaRepository $ideaRepository, TagRepository $tagRepository): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        if (false === $user->isVerified()) {
            return $this->redirectToRoute('profile');
        }

        $educations = $countries = [];
        /** @var UserEducation $education */
        foreach ($user->getEducations() as $s) {
            if (false === is_null($s->getInstitution())) {
                $educations[] = $s;
            } elseif (in_array($s->getCountry(), OpenCountryEnum::getReadableChoices())) {
                $countries[] = $s->getCountry();
            }
        }

        $tagsIdeas = $tagRepository->findAll();
        shuffle($tagsIdeas);

        return $this->render('post/ideas.html.twig', [
            'postsIdeas' => [
                'institutions' => $ideaRepository->findBy(['type' => PostIdeaTypeEnum::INSTITUTION]),
                'city' => $ideaRepository->findBy(['type' => PostIdeaTypeEnum::CITY]),
                'country' => $ideaRepository->findBy(['type' => PostIdeaTypeEnum::COUNTRY]),
            ],
            'tagsIdeas' => $tagsIdeas,
            'okEducations' => $educations,
            'okCountries' => $countries,
        ]);
    }
}
