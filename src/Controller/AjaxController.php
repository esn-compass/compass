<?php

namespace App\Controller;

use App\Entity\Post;
use App\Entity\PostReaction;
use App\Enum\PostReactionTypeEnum;
use App\Repository\PostReactionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class AjaxController extends AbstractController
{
    /**
     * @Route("posts/{id}/react/{type}", name="post_react", methods={"POST"})
     */
    public function ajaxReact(
        Post $post,
        string $type,
        EntityManagerInterface $em,
        PostReactionRepository $prRepo
    ): Response {

        if (false === $this->isGranted('ROLE_USER')) {
            throw new BadRequestHttpException();
        }

        $currentReaction = $post->getReactionOfUser($this->getUser());

        if (false === is_null($currentReaction)) {
            if ($currentReaction->getType() === $type) {
                return new JsonResponse([
                    'code' => 400,
                    'message' => 'Reaction already exists with same type'
                ], 400);
            } elseif (true === PostReactionTypeEnum::isValidValue($type)) {
                $currentReaction->setType($type);
                $em->flush();

                return new JsonResponse([
                    'code' => 200,
                    'message' => 'Reaction modified',
                    'response' => $this->render('_post_reactions.html.twig', ['post' => $post])->getContent()
                ]);
            } elseif ('undo' === $type) {
                $em->remove($currentReaction);
                $em->flush();

                return new JsonResponse([
                    'code' => 200,
                    'message' => 'Reaction deleted',
                    'response' => $this->render('_post_reactions.html.twig', ['post' => $post])->getContent()
                ]);
            }
        } else {
            if (true === PostReactionTypeEnum::isValidValue($type)) {
                $reaction = new PostReaction();
                $reaction->setUser($this->getUser());
                $reaction->setPost($post);
                $reaction->setType($type);

                $em->persist($reaction);
                $em->refresh($post);
                $em->flush();

                return new JsonResponse([
                    'code' => 201,
                    'message' => 'Reaction created',
                    'response' => $this->render('_post_reactions.html.twig', ['post' => $post])->getContent()
                ], 201);
            } elseif ('undo' === $type) {
                return new JsonResponse([
                    'code' => 400,
                    'message' => 'No reaction to delete'
                ], 400);
            }
        }

        return new JsonResponse([
            'code' => 400,
            'message' => 'Bad request'
        ], 400);
    }
}
