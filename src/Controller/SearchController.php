<?php

namespace App\Controller;

use App\Enum\OpenCountryEnum;
use App\Form\SearchFiltersType;
use App\Repository\CityRepository;
use App\Repository\PostRepository;
use App\Repository\TagRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    /**
     * @Route("/search", name="search")
     */
    public function index(
        Request $request,
        TagRepository $tagRepo,
        CityRepository $cityRepo,
        PostRepository $postRepo
    ): Response {
        $filtersForm = $this->createForm(SearchFiltersType::class, null, ['method' => 'GET']);
        $filtersForm->handleRequest($request);

        if (true === empty($filtersForm->getData())) {
            return $this->render('search/index.html.twig', [
                'activeTab' => 'posts',
                'filtersForm' => $filtersForm->createView(),
                'tags' => [],
                'places' => [
                    'cities' => [],
                    'countries' => [],
                ],
                'posts' => [],
            ]);
        }

        $filters = $this->cleanFilters($filtersForm->getData());

        $posts = $postRepo->searchFromFilters($filters);
        foreach ($posts as $post) {
            foreach ($filters['text'] as $part) {
                $post->setTitle(preg_replace('/('.$part.')/i', '¤s¤$1¤/s¤', $post->getTitle()));
                $post->setContent(preg_replace('/('.$part.')/i', '¤s¤$1¤/s¤', $post->getContent()));
            }
        }

        return $this->render('search/index.html.twig', [
            'activeTab' => 'posts',
            'filters' => $filters,
            'filtersForm' => $filtersForm->createView(),
            'tags' => $tagRepo->searchFromNameOrSynonyms($filters['text']),
            'places' => [
                'cities' => $cityRepo->searchFromNameOrSynonyms($filters['text']),
                'countries' => OpenCountryEnum::searchCountries($filters['text']),
            ],
            'posts' => $posts,
        ]);
    }

    /**
     * @param array $filters
     * @return array
     */
    private function cleanFilters(array $filters) : array
    {
        if (false === empty($filters['text'])) {
            $filters['text'] = explode(' ', $filters['text']);
        } else {
            $filters['text'] = [];
        }

        if ($filters['tags'] instanceof ArrayCollection) {
            $filters['tags'] = $filters['tags']->toArray();
        }

        return $filters;
    }
}
