<?php

declare(strict_types=1);

namespace App\Controller;

use App\Enum\ModerationStatusEnum;
use App\Repository\PostRepository;
use App\Repository\TagRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/project")
 */
class ProjectController extends AbstractController
{
    /**
     * @Route(name="project_presentation")
     */
    public function index(PostRepository $postRepo, TagRepository $tagRepo): Response
    {
        return $this->render('project/presentation.html.twig');
    }

    /**
     * @Route("/news", name="project_news")
     */
    public function news(): Response
    {
        return $this->render('project/presentation.html.twig');
    }

    /**
     * @Route("/resources", name="project_resources")
     */
    public function resources(): Response
    {
        return $this->render('project/resources.html.twig');
    }

    /**
     * @Route("/doc/{filename}", name="download_file", requirements={"filename" : ".+\.pdf"})
     */
    public function downloadFile(string $filename): Response
    {
        return $this->file('documents/'.$filename);
    }
}
