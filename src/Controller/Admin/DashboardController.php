<?php

namespace App\Controller\Admin;

use App\Entity\City;
use App\Entity\Comment;
use App\Entity\Institution;
use App\Entity\Post;
use App\Entity\UserEducation;
use App\Entity\Tag;
use App\Entity\User;
use App\Enum\ModerationStatusEnum;
use App\Repository\CommentRepository;
use App\Repository\PostRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    public AdminUrlGenerator $aug;
    public PostRepository $pr;
    public CommentRepository $cr;

    public function __construct(
        AdminUrlGenerator $adminUrlGenerator,
        PostRepository $postRepository,
        CommentRepository $commentRepository
    ) {
        $this->aug = $adminUrlGenerator;
        $this->pr = $postRepository;
        $this->cr = $commentRepository;
    }

    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return $this->redirect($this->aug->setController(UserCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Admin Dashboard');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToRoute('Return to site', 'fa fa-arrow-circle-left', 'homepage', []);
        yield MenuItem::section();
//        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Users', 'fas fa-users', User::class);
        yield MenuItem::linkToCrud('User Educations', 'fas fa-graduation-cap', UserEducation::class);
        yield MenuItem::linkToCrud('Institutions', 'fas fa-school', Institution::class);
        yield MenuItem::linkToCrud('Cities', 'fas fa-city', City::class);
        yield MenuItem::linkToCrud('Tags', 'fas fa-tags', Tag::class);
        yield MenuItem::linkToCrud('Posts', 'fas fa-comment-dots', Post::class)
            ->setQueryParameter('filters[moderationStatus][comparison]', '=')
            ->setQueryParameter('filters[moderationStatus][value]', 'accepted');
        yield MenuItem::linkToCrud('Comments', 'fas fa-comments', Comment::class)
            ->setQueryParameter('filters[moderationStatus][comparison]', '=')
            ->setQueryParameter('filters[moderationStatus][value]', 'accepted');

        $nb = $this->pr->count(['moderationStatus' => [ModerationStatusEnum::WAITING]])
        + $this->cr->count(['moderationStatus' => [ModerationStatusEnum::WAITING]]);
        yield MenuItem::linkToRoute('Moderation', 'fas fa-gavel', 'moderation')
            ->setBadge($nb > 0 ? $nb . ' waiting' : '', 'danger');
    }
}
