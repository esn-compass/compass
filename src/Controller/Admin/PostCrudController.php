<?php

namespace App\Controller\Admin;

use App\Entity\Post;
use App\Enum\ModerationStatusEnum;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CountryField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Filter\ChoiceFilter;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;

class PostCrudController extends ModerableAbstractController
{
    public static function getEntityFqcn(): string
    {
        return Post::class;
    }

    public function configureFields(string $pageName): iterable
    {
        if (Crud::PAGE_DETAIL === $pageName) {
            return [
                Field::new('id'),
                Field::new('title'),
                TextareaField::new('content'),
                AssociationField::new('author'),
                AssociationField::new('institution'),
                AssociationField::new('city'),
                CountryField::new('country'),
                Field::new('createdAt'),
                Field::new('updatedAt'),
                ChoiceField::new('moderationStatus')->setChoices(
                    ModerationStatusEnum::getReadableChoices()
                ),
                AssociationField::new('moderatedBy'),
                DateField::new('moderatedAt'),
            ];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return ['title', 'content', AssociationField::new('tags')];
        }

        return [
            Field::new('id'),
            Field::new('title'),
            TextEditorField::new('content'),
            AssociationField::new('author'),
            DateField::new('createdAt'),
            TextField::new('topic'),
            ChoiceField::new('moderationStatus')->setChoices(ModerationStatusEnum::getReadableChoices()),
        ];
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add(ChoiceFilter::new('moderationStatus')
                ->setChoices(ModerationStatusEnum::getReadableChoices()))
            ->add('author')
            ->add('tags');
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, $this->newCancelModerationAction())
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->remove(Crud::PAGE_INDEX, Action::NEW);
    }
}
