<?php

namespace App\Controller\Admin;

use App\Entity\Comment;
use App\Entity\ModerableTrait;
use App\Entity\Post;
use App\Enum\ModerationStatusEnum;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Filter\ChoiceFilter;
use Symfony\Component\HttpFoundation\Response;

class CommentCrudController extends ModerableAbstractController
{
    public static function getEntityFqcn(): string
    {
        return Comment::class;
    }

    public function configureFields(string $pageName): iterable
    {
        if (Crud::PAGE_EDIT === $pageName) {
            return ['content'];
        }

        return [
            Field::new('id'),
            AssociationField::new('post'),
            TextEditorField::new('content'),
            AssociationField::new('author'),
            DateField::new('createdAt'),
            Field::new('updatedAt')->onlyOnDetail(),
            ChoiceField::new('moderationStatus')->setChoices(ModerationStatusEnum::getReadableChoices()),
            AssociationField::new('moderatedBy')->onlyOnDetail(),
            DateField::new('moderatedAt')->onlyOnDetail()
        ];
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add(ChoiceFilter::new('moderationStatus')
                ->setChoices(ModerationStatusEnum::getReadableChoices()))
            ->add('post')
            ->add('author');
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, $this->newCancelModerationAction())
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->remove(Crud::PAGE_INDEX, Action::NEW);
    }
}
