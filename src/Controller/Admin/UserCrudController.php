<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Enum\SituationEnum;
use App\Enum\UserRoleEnum;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use Symfony\Component\Intl\Countries;
use Symfony\Component\Intl\Languages;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class UserCrudController extends AbstractCrudController
{
    private UrlGeneratorInterface $urlGenerator;

    public function __construct(UrlGeneratorInterface $urlGenerator)
    {
        $this->urlGenerator = $urlGenerator;
    }

    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureFields(string $pageName): iterable
    {
        if (Crud::PAGE_EDIT === $pageName) {
            return [
                Field::new('email'),
                ChoiceField::new('roles')->allowMultipleChoices(true)->setChoices(
                    UserRoleEnum::getReadableChoices()
                ),
                Field::new('firstname'),
                Field::new('lastname'),
                ChoiceField::new('gender')->setChoices([
                    'other' => User::GENDER_OTHER,
                    'female' => User::GENDER_FEMALE,
                    'male' => User::GENDER_MALE,
                ]),
                Field::new('birthdate'),
                ChoiceField::new('homeCountries')->allowMultipleChoices(true)->setChoices(
                    array_flip(Countries::getNames())
                ),
                ChoiceField::new('languages')->allowMultipleChoices(true)->setChoices(
                    array_flip(Languages::getNames())
                ),
                ChoiceField::new('currentSituation')->setChoices(SituationEnum::getReadableChoices()),
                Field::new('isVerified'),
                Field::new('skipModeration')
            ];
        }

        return [
            Field::new('id'),
            Field::new('email'),
            ChoiceField::new('roles')->setChoices(UserRoleEnum::getReadableChoices()),
            Field::new('firstname'),
            Field::new('lastname'),
            ChoiceField::new('gender')->setChoices([
                'other' => User::GENDER_OTHER,
                'female' => User::GENDER_FEMALE,
                'male' => User::GENDER_MALE,
            ]),
            DateField::new('createdAt'),
            BooleanField::new('isVerified')->renderAsSwitch(false),
            BooleanField::new('skipModeration')->hideOnIndex(),
        ];
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('roles')
            ->add('birthdate')
            ->add('homeCountries')
            ->add('createdAt')
//            ->add('lastLogin')
            ->add('isVerified');
    }

    public function configureActions(Actions $actions): Actions
    {
        $impersonate = Action::new('impersonate', 'Impersonate', 'fa fa-user-lock')
            ->linkToUrl(function (User $user): string {
                return $this->urlGenerator->generate(
                    'homepage',
                    ['_switch_user' => $user->getEmail()],
                    UrlGeneratorInterface::ABSOLUTE_URL
                );
            })
            ->displayIf(function (User $user) {
                return $this->isGranted('ROLE_ALLOWED_TO_SWITCH');
            });

        return $actions
            ->remove(Crud::PAGE_INDEX, Action::NEW)
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->add(Crud::PAGE_DETAIL, $impersonate);
    }
}
