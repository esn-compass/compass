<?php

namespace App\Controller\Admin;

use App\Entity\Comment;
use App\Entity\Post;
use App\Enum\ModerationStatusEnum;
use App\Enum\UserRoleEnum;
use App\Repository\CommentRepository;
use App\Repository\PostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Workflow\Exception\LogicException;
use Symfony\Component\Workflow\WorkflowInterface;

class ModerationController extends AbstractController
{
    private WorkflowInterface $moderationStateMachine;

    public function __construct(WorkflowInterface $moderationStateMachine)
    {
        $this->moderationStateMachine = $moderationStateMachine;
    }

    /**
     * @Route("/admin/moderation", name="moderation")
     */
    public function list(PostRepository $postRepo, CommentRepository $commentRepo): Response
    {
        $posts = $postRepo->findBy(['moderationStatus' => [ModerationStatusEnum::WAITING]]);
        $comments = $commentRepo->findBy(['moderationStatus' => [ModerationStatusEnum::WAITING]]);

        return $this->render('admin/moderation.html.twig', [
            'posts' => $posts,
            'comments' => $comments,
        ]);
    }

    /**
     * @Route("/admin/{type}/{id}/action/{action}", name="moderate_action", methods={"POST"})
     */
    public function moderatePost(string $type, int $id, string $action, EntityManagerInterface $em): Response
    {
        $content = null;
        if ($type === 'post') {
            $content = $em->find(Post::class, $id);
        } elseif ($type = 'comment') {
            $content = $em->find(Comment::class, $id);
        }

        if (true === is_null($content)) {
            return $this->json([
                'code' => 404,
                'message' => 'Not found'
            ], 404);
        }

        if (false === $this->isGranted(UserRoleEnum::ROLE_ADMIN)) {
            return $this->json([
                'code' => 403,
                'message' => 'Unauthorized'
            ], 403);
        }

        try {
            $this->moderationStateMachine->apply($content, $action);
            $content->setModeratedBy($this->getUser());
            $content->setModeratedAt(new \DateTime());

            if ('accept' === $action && false === $content->getAuthor()->isSkipModeration()) {
                if (count($content->getAuthor()->getPublicPosts()) + count($content->getAuthor()->getPublicComments())
                    >= 5) {
                    $content->getAuthor()->setSkipModeration(true);
                }
            }
        } catch (LogicException $exception) {
            return $this->json([
                'code' => 400,
                'message' => 'Bad request'
            ], 400);
        }
        $em->flush();

        return $this->json([
            'code' => 200,
            'message' => 'Post moderation status changed',
            'additional_info' => [
                'new_status' => $content->getModerationStatus(),
                'by' => ['id' => $content->getModeratedBy()->getId()],
                'on' => $content->getModeratedAt()->format('Y-m-d H:i:s'),
            ]
        ]);
    }
}
