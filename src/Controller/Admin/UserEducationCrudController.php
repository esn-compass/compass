<?php

namespace App\Controller\Admin;

use App\Entity\UserEducation;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CountryField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;

class UserEducationCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return UserEducation::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('user'),
            AssociationField::new('institution'),
            Field::new('name'),
            CountryField::new('country'),
            Field::new('location'),
            Field::new('startDate'),
            Field::new('endDate'),
            BooleanField::new('local')->renderAsSwitch(false),
        ];
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters->add('user');
    }
}
