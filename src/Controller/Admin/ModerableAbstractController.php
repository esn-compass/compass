<?php

namespace App\Controller\Admin;

use App\Entity\ModerableTrait;
use App\Entity\Post;
use App\Enum\ModerationStatusEnum;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;

abstract class ModerableAbstractController extends AbstractCrudController
{
    public function newCancelModerationAction() : Action
    {
        return Action::new('cancelModeration', 'Cancel moderation', 'fas fa-gavel')
            ->linkToCrudAction('cancelModeration')
            ->displayIf(fn ($entity) => in_array($entity->getModerationStatus(), [
                ModerationStatusEnum::ACCEPTED,
                ModerationStatusEnum::REFUSED
            ]));
    }

    public function cancelModeration(
        AdminContext $context,
        EntityManagerInterface $em,
        AdminUrlGenerator $aug
    ): Response {
        /** @var ModerableTrait $moderable */
        $moderable = $context->getEntity()->getInstance();
        $moderable->setModerationStatus(ModerationStatusEnum::WAITING);
        $em->flush();

        $targetUrl = $aug
            ->setController($moderable instanceof Post ? PostCrudController::class : CommentCrudController::class)
            ->setAction(Crud::PAGE_INDEX)
            ->generateUrl();
        return $this->redirect($targetUrl);
    }
}
