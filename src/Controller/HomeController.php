<?php

declare(strict_types=1);

namespace App\Controller;

use App\Enum\ModerationStatusEnum;
use App\Repository\PostRepository;
use App\Repository\TagRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route(name="homepage")
     */
    public function index(PostRepository $postRepo, TagRepository $tagRepo): Response
    {
        return $this->render('home/index.html.twig', [
            'popularTags' => $tagRepo->findPopularTag(3),
            'lastPosts' => $postRepo->findBy([
                'moderationStatus' => ModerationStatusEnum::ACCEPTED
            ], ['createdAt' => 'DESC'], 4),
        ]);
    }

    /**
     * @Route("/about-project", name="home_project")
     */
    public function project(): Response
    {
        return $this->render('home/project.html.twig');
    }

    /**
     * @Route("/contact-us", name="home_contact_us")
     */
    public function contactUs(): Response
    {
        return $this->render('home/contact_us.html.twig');
    }

    /**
     * @Route("/terms-and-conditions", name="home_terms_and_conditions")
     */
    public function termsAndConditions(): Response
    {
        return $this->render('home/terms_and_conditions.html.twig');
    }

    /**
     * @Route("/charter", name="home_charter")
     */
    public function charter(): Response
    {
        return $this->render('home/charter.html.twig');
    }
    /**
     * @Route("/faq", name="home_faq")
     */
    public function faq(): Response
    {
        return $this->render('home/faq.html.twig');
    }

    /**
     * @Route("/privacy-policy", name="home_privacy_policy")
     */
    public function privacyPolicy(): Response
    {
        return $this->render('home/privacy_policy.html.twig');
    }
}
