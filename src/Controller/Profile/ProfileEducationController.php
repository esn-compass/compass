<?php

namespace App\Controller\Profile;

use App\Entity\User;
use App\Entity\UserEducation;
use App\Enum\OpenCountryEnum;
use App\Form\UserEducationType;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Exception\ForbiddenActionException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Intl\Countries;
use Symfony\Component\Routing\Annotation\Route;

class ProfileEducationController extends AbstractController
{
    /**
     * @Route("/profile/educations", name="profile_educations_manage")
     */
    public function index(): Response
    {
        $educations = $this->getUser()->getEducations();
        return $this->render('profile/educations_list.html.twig', [
            'educations' => $educations,
        ]);
    }

    /**
     * @Route("/profile/educations/new", name="profile_educations_new")
     */
    public function new(Request $request, EntityManagerInterface $em): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        if (false === $user->isVerified()) {
            return $this->redirectToRoute('profile');
        }

        if ($request->query->has('country')) {
            $education = new UserEducation();
            $education->setUser($user);
            $form = $this->createForm(UserEducationType::class, $education, [
                'country' => $request->query->get('country'),
                'force_custom' => $request->query->has('custom'),
            ]);

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $em->persist($education);
                $em->flush();
                return $this->redirectToRoute('profile_educations_manage');
            }

            return $this->render('profile/educations_form.html.twig', [
                'education' => $education,
                'country' => $request->query->get('country'),
                'form' => $form->createView(),
            ]);
        }

        return $this->render('profile/educations_form.html.twig', [
            'europeanCountries' => OpenCountryEnum::getOptions(),
            'otherCountries' => array_diff_key(Countries::getNames(), OpenCountryEnum::getOptions()),
        ]);
    }

    /**
     * @Route("/profile/educations/{id}/edit", name="profile_educations_edit")
     */
    public function edit(UserEducation $education, Request $request, EntityManagerInterface $em): Response
    {
        if ($education->getUser() !== $this->getUser()) {
            $this->addFlash('error', 'Something wrong happened');
            return $this->redirectToRoute('profile_educations_manage');
        }

        $form = $this->createForm(UserEducationType::class, $education);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            return $this->redirectToRoute('profile_educations_manage');
        }

        return $this->render('profile/educations_form.html.twig', [
            'education' => $education,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/profile/educations/{id}/delete", name="profile_educations_delete", requirements={"id"="\d+"}))
     */
    public function delete(UserEducation $education, EntityManagerInterface $em): Response
    {
        if ($education->getUser() === $this->getUser()) {
            $em->remove($education);
            $em->flush();
            $this->addFlash('success', 'Entry has been deleted');
        } else {
            $this->addFlash('error', 'Something wrong happened');
        }

        return $this->redirectToRoute('profile_educations_manage');
    }
}
