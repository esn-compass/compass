<?php

declare(strict_types=1);

namespace App\Controller\Profile;

use App\Entity\SnsInfo;
use App\Entity\User;
use App\Enum\SituationEnum;
use App\Form\ProfileFormType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ProfileController extends AbstractController
{
    /**
     * @Route("/profile/{userId}", name="profile_member", requirements={"userId"="\d+"})
     * @Route("/profile", name="profile")
     */
    public function show(UserRepository $userRepo, int $userId = null): Response
    {
        if (false === is_null($userId)) {
            $user = $userRepo->find($userId);
            if (!$user instanceof User) {
                throw new NotFoundHttpException();
            }
        } else {
            $user = $this->getUser();
        }

        $allStatus = $this->isGranted('ROLE_ADMIN') || $user === $this->getUser();

        return $this->render('profile/show.html.twig', [
            'user' => $user,
            'userPosts' => $allStatus ? $user->getPosts() : $user->getPublicPosts(),
            'userComments' => $allStatus ? $user->getComments() : $user->getPublicComments(),
            'extra' => [
                'currentSituation' => SituationEnum::getLabel($user->getCurrentSituation())
            ]
        ]);
    }

    /**
     * @Route("/profile/edit", name="profile_edit")
     */
    public function edit(Request $request, EntityManagerInterface $em): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        if (true === $user->getSnsInfos()->isEmpty()) {
            $snsInfo = new SnsInfo();
            $user->addSnsInfo($snsInfo);
        }

        $form = $this->createForm(ProfileFormType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            return $this->redirectToRoute('profile');
        }

        return $this->render('profile/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
