<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ContainsTopic extends Constraint
{
    public $noTopicMessage = 'The post should be linked to one topic (institution, city or country).';
    public $tooManyTopicsMessage = 'The post can only be linked to one topic.';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
