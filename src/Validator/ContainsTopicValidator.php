<?php

namespace App\Validator;

use App\Entity\Post;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ContainsTopicValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof ContainsTopic) {
            throw new UnexpectedTypeException($constraint, ContainsTopic::class);
        }

        if (!$value instanceof Post) {
            throw new UnexpectedTypeException($value, Post::class);
        }

        $nbTopics = 0;
        if ($value->getInstitution()) {
            $nbTopics++;
        }
        if ($value->getCity()) {
            $nbTopics++;
        }
        if ($value->getCountry()) {
            $nbTopics++;
        }

        if (is_null($value->getTopic())) {
            $this->context->buildViolation($constraint->noTopicMessage)
                ->addViolation();
        }

        if ($nbTopics > 1) {
            $this->context->buildViolation($constraint->tooManyTopicsMessage)
                ->addViolation();
        }
    }
}
