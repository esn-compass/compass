<?php

namespace App\Command;

use App\Entity\City;
use App\Entity\Institution;
use App\Enum\OpenCountryEnum;
use App\Repository\CityRepository;
use App\Repository\InstitutionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class InstitutionsUpdateCommand extends Command
{
    protected static $defaultName = 'app:institutions:update';
    protected static $defaultDescription = 'Add a short description for your command';

    private HttpClientInterface $client;
    private EntityManagerInterface $em;
    private InstitutionRepository $institutionRepository;
    private CityRepository $cityRepository;
    private AsciiSlugger $slugger;

    public function __construct(
        HttpClientInterface $client,
        EntityManagerInterface $entityManager,
        InstitutionRepository $institutionRepository,
        CityRepository $cityRepository
    ) {
        parent::__construct();

        $this->client = $client;
        $this->em = $entityManager;
        $this->institutionRepository = $institutionRepository;
        $this->cityRepository = $cityRepository;
        $this->slugger = new AsciiSlugger();
    }

    protected function configure(): void
    {
        $this
            ->addOption(
                'country',
                null,
                InputOption::VALUE_OPTIONAL,
                'To update institutions list only for one country'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $country = $input->getOption('country');

        if ($country) {
            if (false === in_array($country, array_keys(OpenCountryEnum::getOptions()))) {
                $io->error('Given country is invalid or not in the open list');
                return Command::INVALID;
            }
            $countries = [$country];
        } else {
            $io->note('The process will be done for all open countries');
            $countries = array_keys(OpenCountryEnum::getOptions());
        }

        foreach ($countries as $c) {
            $io->info(sprintf('Process starting for: %s', OpenCountryEnum::getOptions()[$c]));

            $added = $updated = [];

            try {
                $response = $this->client->request('GET', 'https://hei.api.uni-foundation.eu/institutions/' . $c);

                if (200 !== $response->getStatusCode()) {
                    $io->warning(sprintf('Status code error: %s', $response->getStatusCode()));
                    continue;
                }

                foreach ($response->toArray()['data'] as $i) {
                    $existingObject = $this->institutionRepository->findOneByExternalId($i['id']);

                    if ($existingObject instanceof Institution) {
                        // Todo: decide what to update
                        $updated[] = $i['id'];
                    } else {
                        $newInst = new Institution();
                        $newInst->setName($i['attributes']['label']);
                        $newInst->setCity($this->findOrCreateCity($i['attributes']['city'], $c));
                        $slug = strtolower($this->slugger->slug($i['attributes']['label']));
                        $newInst->setSlug(substr($slug, 0, 100));
                        $newInst->setExternalId($i['id']);
                        $this->em->persist($newInst);
                        $added[] = $i['id'];
                    }
                }

                $this->em->flush();

                $institutionsIds = array_map(function ($e) {
                    return $e->getExternalId();
                }, $this->institutionRepository->findByCountry($c));

                $io->info(sprintf(
                    'Process for %s done. %d added, %d ignored and %d should be removed.',
                    OpenCountryEnum::getOptions()[$c],
                    count($added),
                    count($updated),
                    count(array_diff($institutionsIds, array_merge($updated, $added)))
                ));
            } catch (\Exception $e) {
                $io->error(sprintf('An error occurred: %s', $e->getMessage()));
                return Command::FAILURE;
            }
        }

        $io->success('Process finished');
        return Command::SUCCESS;
    }

    private function findOrCreateCity(string $cityNames, string $country) : City
    {
        $cityNames = explode('|', explode('/', $cityNames)[0]);
        $slug = strtolower($this->slugger->slug(trim($cityNames[0])));

        $city = $this->cityRepository->findOneBySlug($slug);
        if ($city instanceof City) {
            return $city;
        }

        $city = new City();
        $city->setName($cityNames[0]);
        $city->setCountry($country);
        $city->setSlug($slug);
        if (count($cityNames) > 1) {
            unset($cityNames[0]);
            $city->setSynonyms($cityNames);
        }

        $this->em->persist($city);
        $this->em->flush($city);

        return $city;
    }
}
