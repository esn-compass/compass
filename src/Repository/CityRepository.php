<?php

namespace App\Repository;

use App\Entity\City;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method City|null find($id, $lockMode = null, $lockVersion = null)
 * @method City|null findOneBy(array $criteria, array $orderBy = null)
 * @method City[]    findAll()
 * @method City[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, City::class);
    }

    /**
     * @return City[]
     */
    public function searchFromNameOrSynonyms(array $textParts): array
    {
        if (true === empty($textParts)) {
            return [];
        }

        $qb = $this->createQueryBuilder('c');

        foreach ($textParts as $i => $part) {
            $qb
                ->orWhere("c.name LIKE :part_$i")
                ->orWhere("c.synonyms LIKE :part_$i")
                ->setParameter("part_$i", "%$part%");
        }

        return $qb
            ->orderBy('c.name', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
