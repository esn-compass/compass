<?php

namespace App\Repository;

use App\Entity\City;
use App\Entity\Institution;
use App\Entity\Post;
use App\Enum\ModerationStatusEnum;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Post::class);
    }

    /**
    * @return Post[]
    */
    public function findAcceptedByTagsNames(array $tags, array $orderBy = ['createdAt', 'DESC']): array
    {
        return $this->createQueryBuilder('p')
            ->innerJoin('p.tags', 't')
            ->where('t.name IN (:tags)')
            ->setParameter('tags', $tags)
            ->andWhere('p.moderationStatus = :modStatus')
            ->setParameter('modStatus', ModerationStatusEnum::ACCEPTED)
            ->groupBy('p.id')
            ->having('COUNT(DISTINCT t.id) = '.count($tags))
            ->orderBy('p.' . $orderBy[0], $orderBy[1])
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Post[]
     */
    public function searchFromFilters(array $filters, array $orderBy = ['createdAt', 'DESC']): array
    {
        $qb = $this->createQueryBuilder('p');

        if (true === isset($filters['text']) && false === empty($filters['text'])) {
            $qb = $this->addTextFilter($qb, $filters['text']);
        }

        if (true === isset($filters['place']) && false === empty($filters['place'])) {
            $qb = $this->addPlaceFilter($qb, $filters['place']);
        }

        if (true === isset($filters['tags']) && false === empty($filters['tags'])) {
            $qb = $this->addTagsFilter($qb, $filters['tags']);
        }

        return $qb
            ->andWhere('p.moderationStatus = :modStatus')
            ->setParameter('modStatus', ModerationStatusEnum::ACCEPTED)
            ->orderBy('p.' . $orderBy[0], $orderBy[1])
            ->getQuery()
            ->getResult();
    }

    /**
     * @param QueryBuilder $qb
     * @param array $words
     * @return QueryBuilder
     */
    private function addTextFilter(QueryBuilder $qb, array $words) : QueryBuilder
    {
        $textOr = $qb->expr()->orX();
        foreach ($words as $i => $part) {
            // ignore small words
            if (strlen($part) < 3) {
                continue;
            }
            $textOr->add($qb->expr()->like('p.title', ":part_$i"));
            $textOr->add($qb->expr()->like('p.content', ":part_$i"));
            $qb->setParameter("part_$i", "%$part%");
        }
        return $qb->andWhere($textOr);
    }

    /**
     * @param QueryBuilder $qb
     * @param $place
     * @return QueryBuilder
     */
    private function addPlaceFilter(QueryBuilder $qb, $place) : QueryBuilder
    {
        $qb->leftJoin('p.city', 'p_c')
            ->leftJoin('p.institution', 'i')
            ->leftJoin('i.city', 'i_c');

        if ($place instanceof Institution) {
            $qb->andWhere("p.institution = :place");
        } elseif ($place instanceof City) {
            $qb->andWhere($qb->expr()->orX(
                $qb->expr()->eq('p.city', ':place'),
                $qb->expr()->eq('i.city', ':place'),
                $qb->expr()->eq('i_c.bigCity', ':place')
            ));
        } else {
            $qb->andWhere($qb->expr()->orX(
                $qb->expr()->eq('p.country', ':place'),
                $qb->expr()->eq('p_c.country', ':place'),
                $qb->expr()->eq('i_c.country', ':place')
            ));
        }
        return $qb->setParameter("place", $place);
    }

    /**
     * @param QueryBuilder $qb
     * @param array $tags
     * @return void
     */
    private function addTagsFilter(QueryBuilder $qb, array $tags) : QueryBuilder
    {
        $tagsOr = $qb->expr()->orX();
        foreach ($tags as $i => $tag) {
            $tagsOr->add($qb->expr()->isMemberOf(":tag_$i", 'p.tags'));
            $qb->setParameter("tag_$i", $tag);
        }
        return $qb->andWhere($tagsOr);
    }
}
