<?php

namespace App\Repository;

use App\Entity\Post;
use App\Entity\Tag;
use App\Enum\ModerationStatusEnum;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Tag|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tag|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tag[]    findAll()
 * @method Tag[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TagRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tag::class);
    }

    /**
     * @return Tag[]
     */
    public function findPopularTag(int $limit = 3): array
    {
        return $this->createQueryBuilder('t')
            ->addSelect('COUNT(DISTINCT p.id) as NB_POSTS')
            ->join(Post::class, 'p', Join::WITH, 't MEMBER OF p.tags')
            ->where('p.moderationStatus = :modStatus')
            ->setParameter('modStatus', ModerationStatusEnum::ACCEPTED)
            ->groupBy('t.id')
            ->orderBy('NB_POSTS', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Tag[]
     */
    public function searchFromNameOrSynonyms(array $textParts): array
    {
        if (empty($textParts)) {
            return [];
        }

        $qb = $this->createQueryBuilder('t');

        foreach ($textParts as $i => $part) {
            $qb
                ->orWhere("t.name LIKE :part_$i")
                ->orWhere("t.synonyms LIKE :part_$i")
                ->setParameter("part_$i", "%$part%");
        }

        return $qb
            ->orderBy('t.name', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /*
    public function findOneBySomeField($value): ?Tag
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
