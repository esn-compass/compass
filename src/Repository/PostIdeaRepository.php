<?php

namespace App\Repository;

use App\Entity\PostIdea;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PostIdea|null find($id, $lockMode = null, $lockVersion = null)
 * @method PostIdea|null findOneBy(array $criteria, array $orderBy = null)
 * @method PostIdea[]    findAll()
 * @method PostIdea[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostIdeaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PostIdea::class);
    }

    // /**
    //  * @return PostIdea[] Returns an array of PostIdea objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PostIdea
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
