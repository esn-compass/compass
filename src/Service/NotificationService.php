<?php

namespace App\Service;

use App\Entity\User;
use App\Enum\ModerationStatusEnum;
use App\Repository\CommentRepository;
use App\Repository\PostRepository;

class NotificationService
{
    private PostRepository $postRepository;
    private CommentRepository $commentRepository;

    public function __construct(PostRepository $postRepository, CommentRepository $commentRepository)
    {
        $this->postRepository = $postRepository;
        $this->commentRepository = $commentRepository;
    }

    public function getUserDashboardNotificationsCount(User $user): int
    {
        $count = 0;

        if (true === in_array('ROLE_ADMIN', $user->getRoles())) {
            $count += $this->postRepository->count(['moderationStatus' => [ModerationStatusEnum::WAITING]]);
            $count += $this->commentRepository->count(['moderationStatus' => [ModerationStatusEnum::WAITING]]);
        }

        return $count;
    }
}
