<?php

namespace App\Form;

use App\Entity\Post;
use App\Entity\User;
use App\Entity\UserEducation;
use App\Enum\OpenCountryEnum;
use App\Repository\CityRepository;
use App\Repository\InstitutionRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class PostType extends AbstractType
{
    private Security $security;
    private CityRepository $cityRepository;
    private InstitutionRepository $institutionRepository;

    public function __construct(Security $security, CityRepository $cityRepo, InstitutionRepository  $institutionRepo)
    {
        $this->security = $security;
        $this->cityRepository = $cityRepo;
        $this->institutionRepository = $institutionRepo;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title')
            ->add('content')
            ->add('tags', null, [
                'choice_attr' => function ($choice) {
                    return ['data-synonyms' => implode(';', $choice->getSynonyms())];
                },
            ])
            ->add('subject', ChoiceType::class, [
                'mapped' => false,
                'required' => true,
                'choices' => $this->generatePossibleSubjectsForCurrentUser()
            ])
            ->addEventListener(FormEvents::SUBMIT, [$this, 'onSubmit'])
        ;
    }

    private function generatePossibleSubjectsForCurrentUser(): array
    {
        if (!$this->security->getUser() instanceof User) {
            return [];
        }

        $countries = $cities = $institutions = [];
        foreach ($this->security->getUser()->getEducations() as $education) {
            /** @var UserEducation $education */

            if (false === is_null($education->getInstitution())) {
                $c = $education->getInstitution()->getCity();
                $countries[$c->getCountry()] = $c->getCountry();
                $cities['city-'.$c->getId()] = $c->getName();
                if ($c->hasBigCity()) {
                    $cities['city-'.$c->getBigCity()->getId()] = $c->getBigCity()->getName();
                }
                $institutions['inst-'.$education->getInstitution()->getId()] = $education->getInstitution()->getName();
            }

            if (false === is_null($education->getCountry())) {
                $countries[$education->getCountry()] = $education->getCountry();
            }
        }

        $countries = array_intersect_key(OpenCountryEnum::getOptions(), $countries);

        return [
            '' => null,
            'Institutions' => array_flip($institutions),
            'Cities' => array_flip($cities),
            'Countries' => array_flip($countries),
        ];
    }

    public function onSubmit(FormEvent $event): void
    {
        /** @var Post $post */
        $post = $event->getData();
        $form = $event->getForm();

        if (false === is_null($form->get('subject')->getData())) {
            $subject = explode('-', $form->get('subject')->getData());
            if (str_starts_with($subject[0], 'city')) {
                $post->setCity($this->cityRepository->find($subject[1]));
            } elseif (str_starts_with($subject[0], 'inst')) {
                $post->setInstitution($this->institutionRepository->find($subject[1]));
            } else {
                $post->setCountry($subject[0]);
            }
        }

        $event->setData($post);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Post::class,
        ]);
    }
}
