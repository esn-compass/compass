<?php

namespace App\Form;

use App\Entity\City;
use App\Entity\Institution;
use App\Entity\Tag;
use App\Enum\OpenCountryEnum;
use App\Repository\CityRepository;
use App\Repository\InstitutionRepository;
use App\Repository\TagRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchFiltersType extends AbstractType
{
    private TagRepository $tagRepository;
    private CityRepository $cityRepository;
    private InstitutionRepository $institutionRepository;

    public function __construct(
        TagRepository $tagRepository,
        CityRepository $cityRepository,
        InstitutionRepository $institutionRepository
    ) {
        $this->tagRepository = $tagRepository;
        $this->cityRepository = $cityRepository;
        $this->institutionRepository = $institutionRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $tags = [];
        foreach ($this->tagRepository->findAll() as $tag) {
            $tags[$tag->getName()] = $tag;
        }

        $builder
            ->add('text', TextType::class, [
                'required' => false,
            ])
            ->add('place', ChoiceType::class, [
                'required' => false,
                'choices' => $this->generatePossibleSubjects(),
                'choice_value' => function ($choice) {
                    if ($choice instanceof Institution) {
                        return 'i' . $choice->getId();
                    } elseif ($choice instanceof City) {
                        return 'c' . $choice->getId();
                    } else {
                        return $choice;
                    }
                }
            ])
            ->add('tags', EntityType::class, [
                'required' => false,
                'class' => Tag::class,
                'multiple' => true,
                'choices' => $tags,
                'choice_attr' => function ($choice) {
                    return ['data-synonyms' => implode(';', $choice->getSynonyms())];
                },
                'choice_value' => function ($choice) {
                    return $choice->getName();
                }
            ])
        ;
    }

    private function generatePossibleSubjects(): array
    {
        $cities = $institutions = [];

        foreach ($this->cityRepository->findAll() as $c) {
            $cities[$c->getName() . ' (' . $c->getCountry() . ')'] = $c;
        }

        foreach ($this->institutionRepository->findAll() as $i) {
            $institutions[$i->getName()] = $i;
        }

        return [
            '' => null,
            'Countries' => array_flip(OpenCountryEnum::getOptions()),
            'Cities' => $cities,
            'Institutions' => $institutions,
        ];
    }

    public function getBlockPrefix(): string
    {
        return '';
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ]);
    }
}
