<?php

namespace App\Form;

use App\Entity\User;
use App\Enum\SituationEnum;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\LanguageType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class ProfileFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstname')
            ->add('lastname')
            ->add('birthdate', BirthdayType::class, [
                'widget' => 'single_text',
            ])
            ->add('gender', ChoiceType::class, [
                'choices' => [
                    'other' => User::GENDER_OTHER,
                    'female' => User::GENDER_FEMALE,
                    'male' => User::GENDER_MALE,
                ],
                'constraints' => [
                    new NotNull()
                ]
            ])
            ->add('homeCountries', CountryType::class, [
                'multiple' => true,
            ])
            ->add('languages', LanguageType::class, [
                'multiple' => true,
            ])
            ->add('currentSituation', ChoiceType::class, [
                'choices' => SituationEnum::getReadableChoices(),
                'required' => false,
            ])
            //            ->add('snsVisibility', ChoiceType::class, [
            //                'choices' => SnsVisibilityEnum::getReadableChoices()
            //            ])
            ->add('snsInfos', CollectionType::class, [
                'label' => 'Social network others can contact you to',
                'entry_type' => SnsInfoType::class,
                'by_reference' => false,
                'required' => false,
                'allow_add' => true,
                'allow_delete' => true
            ])
            ->add('imageFile', VichImageType::class, [
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '500k',
                        'mimeTypes' => [
                            'image/png',
                            'image/jpg',
                            'image/jpeg'
                        ],
                        'mimeTypesMessage' => 'Please upload a valid JPG/PNG file, maxsize 1024k',
                    ])

                ]
            ])


            ->addEventListener(
                FormEvents::PRE_SUBMIT,
                [$this, 'onPreSubmit']
            );
    }

    public function onPreSubmit(FormEvent $event): void
    {
        // remove blank SNS info from the submitted form
        $profile = $event->getData();
        if (isset($profile['snsInfos'])) {
            foreach ($profile['snsInfos'] as $key => $snsInfo) {
                if (true === empty($snsInfo['content'])) {
                    unset($profile['snsInfos'][$key]);
                }
            }
        }
        $event->setData($profile);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
