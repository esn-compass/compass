<?php

namespace App\Form;

use App\Entity\Institution;
use App\Entity\UserEducation;
use App\Repository\InstitutionRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Intl\Countries;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserEducationType extends AbstractType
{
    private InstitutionRepository $institutionRepo;

    private array $months = [
        'January'   => '01',
        'February'  => '02',
        'March'     => '03',
        'April'     => '04',
        'May'       => '05',
        'June'      => '06',
        'July'      => '07',
        'August'    => '08',
        'September' => '09',
        'October'   => '10',
        'November'  => '11',
        'December'  => '12'
    ];

    public function __construct(InstitutionRepository $institutionRepository)
    {
        $this->institutionRepo = $institutionRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var UserEducation $education */
        $education = $builder->getData();

        // NEW entity form
        if (true === is_null($education->getId())) {
            $builder->add('country', ChoiceType::class, [
                'choices' => [Countries::getName($options['country']) => $options['country']]
            ]);

            $institutions = [];
            if (false === is_null($options['country'])) {
                $institutions = $this->institutionRepo->findByCountry($options['country']);
            }

            if (false === empty($institutions) && false === $options['force_custom']) {
                $builder->add('institution', EntityType::class, [
                    'class' => Institution::class,
                    'choices' => $institutions,
                    'required' => true,
                ]);
            } else {
                $builder
                    ->add('location', TextType::class, ['required' => true])
                    ->add('name', TextType::class, ['required' => true])
                ;
            }
        } else { // EDIT entity form
            $startDate = explode('-', $education->getStartDate());
            $endDate = explode('-', $education->getEndDate());

            if (true === is_null($education->getInstitution())) {
                $builder
                    ->add('location', TextType::class, ['required' => true])
                    ->add('name', TextType::class, ['required' => true])
                ;
            }
        }

        $builder
            ->add('startDateYear', ChoiceType::class, [
                'mapped' => false,
                'label' => 'Date of start : year',
                'choices' => array_combine(range(2010, 2023), range(2010, 2023)),
                'data' => isset($startDate) ? $startDate[0] : '2022',
            ])
            ->add('startDateMonth', ChoiceType::class, [
                'mapped' => false,
                'label' => 'month',
                'choices' => $this->months,
                'data' => isset($startDate) ? $startDate[1] : '09',
            ])
            ->add('endDateYear', ChoiceType::class, [
                'mapped' => false,
                'label' => 'Date of end : year',
                'choices' => array_combine(range(2010, 2025), range(2010, 2025)),
                'data' => isset($endDate) ? $endDate[0] : '2022',
            ])
            ->add('endDateMonth', ChoiceType::class, [
                'mapped' => false,
                'label' => 'month',
                'choices' => $this->months,
                'data' => isset($endDate) ? $endDate[1] : '05',
            ])
            ->add('local', null, [
                'label' => 'I was a local student there',
            ])
            ->addEventListener(FormEvents::SUBMIT, [$this, 'onSubmit'])
        ;
    }

    public function onSubmit(FormEvent $event): void
    {
        /** @var UserEducation $education */
        $education = $event->getData();
        $form = $event->getForm();

        $education->setStartDate(
            ($form->get('startDateYear')->getData() . '-' . $form->get('startDateMonth')->getData())
        );
        $education->setEndDate(
            ($form->get('endDateYear')->getData() . '-' . $form->get('endDateMonth')->getData())
        );

        $event->setData($education);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => UserEducation::class,
            'country' => null,
            'force_custom' => false,
        ]);
    }
}
