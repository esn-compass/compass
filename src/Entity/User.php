<?php

namespace App\Entity;

use App\Enum\SnsVisibilityEnum;
use App\Enum\UserStatusEnum;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Serializable;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 * @Vich\Uploadable
 */
class User implements UserInterface, PasswordAuthenticatedUserInterface, Serializable
{
    use TimestampableEntity;

    const GENDER_FEMALE = 0;
    const GENDER_MALE = 1;
    const GENDER_OTHER = 2;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private ?string $email;

    /**
     * @ORM\Column(type="json")
     */
    private array $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private string $password;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private ?string $firstname;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private ?string $lastname = null;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private ?string $username = null;

    /**
     * @ORM\Column(type="smallint")
     */
    private ?int $gender;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Range(
     *     min = "100 years ago",
     *     max = "15 years ago",
     *     minMessage = "Are you really that old ?",
     *     maxMessage = "You have to be older than 15 years old to be here"
     * )
     */
    private ?\DateTime $birthdate = null;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private array $homeCountries = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private array $languages = [];

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $currentSituation = null;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private string $snsVisibility = SnsVisibilityEnum::PUBLIC;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private string $status = UserStatusEnum::SAVED;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?\DateTimeInterface $lastLogin = null;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isVerified = false;

    /**
     * @ORM\OneToMany(targetEntity=SnsInfo::class, mappedBy="user", cascade={"persist"}, orphanRemoval=true)
     */
    private Collection $snsInfos;

    /**
     * @ORM\OneToMany(targetEntity=UserEducation::class, mappedBy="user", cascade={"persist"}, orphanRemoval=true)
     * @ORM\OrderBy({"startDate" = "ASC"})
     */
    private Collection $educations;

    /**
     * @ORM\OneToMany(targetEntity=Post::class, mappedBy="author")
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private Collection $posts;

    /**
     * @ORM\OneToMany(targetEntity=PostReaction::class, mappedBy="user", orphanRemoval=true)
     */
    private Collection $reactions;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="author", orphanRemoval=true)
     */
    private Collection $comments;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $skipModeration = false;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageName;

    /**
     * @Vich\UploadableField(mapping="user_image", fileNameProperty="imageName")
     * @Assert\Image(
     *     minWidth = 100,
     *     maxWidth = 500,
     *     minHeight = 100,
     *     maxHeight = 500
     * )
     */
    private ?File $imageFile = null;


    public function __construct()
    {
        $this->snsInfos = new ArrayCollection();
        $this->educations = new ArrayCollection();
        $this->posts = new ArrayCollection();
        $this->reactions = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->firstname . ' #' .  $this->id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(?string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(?string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getGender(): ?int
    {
        return $this->gender;
    }

    public function setGender(int $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getBirthdate(): ?\DateTime
    {
        return $this->birthdate;
    }

    public function setBirthdate(?\DateTime $birthdate): self
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    public function getAge(): ?string
    {
        $now = new \DateTime();
        $age = $now->diff($this->getBirthdate());
        return $age->y;
    }

    public function getHomeCountries(): ?array
    {
        return $this->homeCountries;
    }

    public function setHomeCountries(?array $homeCountries): self
    {
        $this->homeCountries = $homeCountries;

        return $this;
    }

    public function getLanguages(): ?array
    {
        return $this->languages;
    }

    public function setLanguages(?array $languages): self
    {
        $this->languages = $languages;

        return $this;
    }

    public function getCurrentSituation(): ?string
    {
        return $this->currentSituation;
    }

    public function setCurrentSituation(?string $currentSituation): self
    {
        $this->currentSituation = $currentSituation;

        return $this;
    }

    public function getSnsVisibility(): ?string
    {
        return $this->snsVisibility;
    }

    public function setSnsVisibility(string $snsVisibility): self
    {
        $this->snsVisibility = $snsVisibility;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getLastLogin(): ?\DateTimeInterface
    {
        return $this->lastLogin;
    }

    public function setLastLogin(?\DateTimeInterface $lastLogin): self
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    /**
     * @return Collection|SnsInfo[]
     */
    public function getSnsInfos(): Collection
    {
        return $this->snsInfos;
    }

    public function addSnsInfo(SnsInfo $snsInfo): self
    {
        if (!$this->snsInfos->contains($snsInfo)) {
            $this->snsInfos[] = $snsInfo;
            $snsInfo->setUser($this);
        }

        return $this;
    }

    public function removeSnsInfo(SnsInfo $snsInfo): self
    {
        if ($this->snsInfos->removeElement($snsInfo)) {
            // set the owning side to null (unless already changed)
            if ($snsInfo->getUser() === $this) {
                $snsInfo->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserEducation[]
     */
    public function getEducations(): Collection
    {
        return $this->educations;
    }

    public function addEducation(UserEducation $education): self
    {
        if (!$this->educations->contains($education)) {
            $this->educations[] = $education;
            $education->setUser($this);
        }

        return $this;
    }

    public function removeEducation(UserEducation $education): self
    {
        if ($this->educations->removeElement($education)) {
            // set the owning side to null (unless already changed)
            if ($education->getUser() === $this) {
                $education->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Post[]
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    /**
     * @return Post[]
     */
    public function getPublicPosts(): array
    {
        return array_filter($this->posts->toArray(), function (Post $p) {
            return $p->isPublic();
        });
    }

    /**
     * @return Collection|PostReaction[]
     */
    public function getReactions(): Collection
    {
        return $this->reactions;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    /**
     * @return Comment[]
     */
    public function getPublicComments(): array
    {
        return array_filter($this->comments->toArray(), function (Comment $c) {
            return $c->isPublic();
        });
    }

    /**
     * @return bool
     */
    public function isSkipModeration(): ?bool
    {
        return $this->skipModeration;
    }

    /**
     * @param bool $skipModeration
     * @return $this
     */
    public function setSkipModeration(bool $skipModeration): self
    {
        $this->skipModeration = $skipModeration;

        return $this;
    }

    public function getImageName(): ?string
    {
        return $this->imageName;
    }

    public function setImageName(?string $imageName): self
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $imageFile
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }


    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->email,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->email,
            $this->password,
            // see section on salt below
            // $this->salt
        ) = unserialize($serialized);
    }
}
