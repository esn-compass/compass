<?php

namespace App\Entity;

use App\Enum\ModerationStatusEnum;

trait ModerableTrait
{
    /**
     * @ORM\Column(type="string", length=50)
     */
    private string $moderationStatus = ModerationStatusEnum::WAITING;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?\DateTimeInterface $moderatedAt;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private ?User $moderatedBy;


    public function getModerationStatus(): ?string
    {
        return $this->moderationStatus;
    }

    public function setModerationStatus(string $moderationStatus): self
    {
        $this->moderationStatus = $moderationStatus;

        return $this;
    }

    public function isPublic(): bool
    {
        return $this->moderationStatus === ModerationStatusEnum::ACCEPTED;
    }

    public function getModeratedAt(): ?\DateTimeInterface
    {
        return $this->moderatedAt;
    }

    public function setModeratedAt(?\DateTimeInterface $moderatedAt): self
    {
        $this->moderatedAt = $moderatedAt;

        return $this;
    }

    public function getModeratedBy(): ?User
    {
        return $this->moderatedBy;
    }

    public function setModeratedBy(?User $moderatedBy): self
    {
        $this->moderatedBy = $moderatedBy;

        return $this;
    }
}
