<?php

namespace App\Entity;

use App\Enum\PostTypeEnum;
use App\Repository\PostRepository;
use App\Validator as CustomAssert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=PostRepository::class)
 * @CustomAssert\ContainsTopic
 */
class Post
{
    use ModerableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="posts")
     * @ORM\JoinColumn(nullable=false)
     */
    private User $author;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private string $type = PostTypeEnum::POST;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $title;

    /**
     * @ORM\Column(type="text")
     */
    private ?string $content;

    /**
     * @ORM\ManyToMany(targetEntity=Tag::class, inversedBy="posts")
     * @Assert\Count(min=1, minMessage="A post should have at least one tag.")
     */
    private Collection $tags;

    /**
     * @ORM\ManyToOne(targetEntity=Institution::class, inversedBy="posts")
     */
    private ?Institution $institution = null;

    /**
     * @ORM\ManyToOne(targetEntity=City::class, inversedBy="posts")
     */
    private ?City $city = null;

    /**
     * @ORM\Column(type="string", length=2, nullable=true)
     * @Assert\Choice(callback={"App\Enum\OpenCountryEnum", "getReadableChoices"})
     */
    private ?string $country = null;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private \DateTimeImmutable $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?\DateTimeInterface $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity=PostReaction::class, mappedBy="post", orphanRemoval=true)
     */
    private Collection $reactions;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="post", orphanRemoval=true)
     */
    private Collection $comments;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
        $this->createdAt = new \DateTimeImmutable();
        $this->reactions = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        $this->tags->removeElement($tag);

        return $this;
    }

    public function getTopic()
    {
        if ($this->institution) {
            return $this->institution;
        } elseif ($this->city) {
            return $this->city;
        } else {
            return $this->country;
        }
    }

    public function getInstitution(): ?Institution
    {
        return $this->institution;
    }

    public function setInstitution(?Institution $institution): self
    {
        $this->institution = $institution;

        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection|PostReaction[]
     */
    public function getReactions(): Collection
    {
        return $this->reactions;
    }

    public function getReactionOfUser(?User $user): ?PostReaction
    {
        if ($user instanceof User) {
            foreach ($this->reactions as $reaction) {
                if ($reaction->getUser() === $user) {
                    return $reaction;
                }
            }
        }
        return null;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    /**
     * @return Comment[]
     */
    public function getPublicComments(): array
    {
        return array_filter($this->comments->toArray(), function (Comment $c) {
            return $c->isPublic();
        });
    }

    /**
     * @param User|null $user
     * @return array|Comment[]
     */
    public function getCommentsOfUser(?User $user): array
    {
        $comments = [];
        if ($user instanceof User) {
            foreach ($this->comments as $comment) {
                if ($comment->getAuthor() === $user) {
                    $comments[] = $comment;
                }
            }
        }
        return $comments;
    }

    public function __toString()
    {
        return '#' . $this->id . ' ' . $this->title;
    }
}
