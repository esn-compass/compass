<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Entity\Post;
use App\Entity\PostReaction;
use App\Enum\ModerationStatusEnum;
use App\Enum\PostReactionTypeEnum;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class PostFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {

        $faker = Factory::create();

        $posts = [
            'Humbert' => [
                ['The best neighborhood in Lille', 8, ['accommodation'], 'city_lille'],
                ['The best welsh in Lille', 8, ['food'], 'city_lille'],
                ['Classes in Institut Mines-Télécom Nord Europe', 8, ['studentlife'],
                    'institution_institut-mines-telecom-nord-europe']
            ],
            'Dupont' => [
                ['Student housing in Universite Paris 8 Vincennes Saint-Denis', 8, ['accommodation'],
                    'institution_universite-paris-8-vincennes-saint-denis'],
            ],
            'Torres' => [
                ['My colocation in Madrid', 8, ['accommodation'], 'city_madrid'],
                ['The best tapas in the city center of Madrid', 8, ['goingout'], 'city_madrid'],
                ['The dialect of Marseille and around', 8, ['culture'], 'city_marseille'],
                ['You have to try Bouillabaisse', 8, ['food'], 'city_marseille'],
                ['My mobility in France', 8, ['studentlife'], 'FR'],
            ],
            'Schwartz' => [
                ['Where to stay for a semester in Gamla Stan, Stockholm', 8, ['accommodation'], 'city_stockholm'],
                ['One year in Karolinska Institutet', 8, ['studentlife'], 'institution_karolinska-institutet'],
                ['Swedish people love the fika', 8, ['food'], 'SE'],
            ],
            'Oliveira' => [
                ['Where to get mobile contract in Portugal', 8, ['admin'], 'PT'],
                ['How to find a cheap place in Porto', 8, ['accommodation'], 'city_porto'],
                ['Pasteis de nata to bring back home', 8, ['food'], 'city_porto'],
                ['How to speak like a real portuguese', 8, ['culture'], 'city_porto'],
                ['My favorite restaurant of Sevilla', 8, ['goingout'], 'city_sevilla'],
                ['Where to practice portuguese in Sevilla', 8, ['culture'], 'city_sevilla'],
                ['Report of one semester in Sevilla as a foreign student', 8, ['studentlife'], 'city_sevilla'],
            ],
        ];

        foreach ($posts as $userLastname => $postGroup) {
            foreach ($postGroup as $item) {
                $post = new Post();
                $post->setAuthor($this->getReference('user_' . $userLastname));
                $post->setTitle($item[0]);
                $post->setContent($faker->paragraph($item[1], true));
                foreach ($item[2] as $tag) {
                    $post->addTag($this->getReference('tag_' . $tag));
                }
                if (false !== strpos($item[3], 'institution')) {
                    $post->setInstitution($this->getReference($item[3]));
                } elseif (false !== strpos($item[3], 'city')) {
                    $post->setCity($this->getReference($item[3]));
                } else {
                    $post->setCountry($item[3]);
                }
                $post->setModerationStatus(ModerationStatusEnum::getRandomOption());

                // add reactions from random other users
                $reactioners = array_rand($posts, random_int(2, 5));
                foreach ($reactioners as $reactioner) {
                    if ($reactioner === $userLastname) {
                        continue;
                    }
                    $reaction = new PostReaction();
                    $reaction->setUser($this->getReference('user_' . $reactioner));
                    $reaction->setPost($post);
                    $reaction->setType(PostReactionTypeEnum::getRandomOption());
                    $manager->persist($reaction);

                    $comment = new Comment();
                    $comment->setAuthor($this->getReference('user_' . $reactioner));
                    $comment->setPost($post);
                    $comment->setContent($faker->text(150));
                    $comment->setModerationStatus(ModerationStatusEnum::getRandomOption());
                    $manager->persist($comment);
                }

                $manager->persist($post);
            }
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            CityFixtures::class,
            InstitutionFixtures::class,
            UserFixtures::class,
            TagFixtures::class,
        ];
    }
}
