<?php

namespace App\DataFixtures;

use App\Entity\City;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CityFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $cities = [
            ['Lille', 'lille', [], 'FR', [
                ['Villeneuve d\'Ascq', 'villeneuve-dascq', [], 'FR', []]
            ]],
            ['Marseille', 'marseille', [], 'FR', [
                ['Aix-en-Provence', 'aix-en-provence', [], 'FR', []]
            ]],
            ['Paris', 'paris', [], 'FR', [
                ['Saint-Denis', 'saint-denis', [], 'FR', []]
            ]],
            ['Madrid', 'madrid', [], 'ES', []],
            ['Sevilla', 'sevilla', ['Seville'], 'ES', []],
            ['Stockholm', 'stockholm', [], 'SE', []],
            ['Porto', 'porto', [], 'PT', []]
        ];

        foreach ($cities as $c) {
            $city = new City();
            $city->setName($c[0]);
            $city->setSlug($c[1]);
            $city->setSynonyms($c[2]);
            $city->setCountry($c[3]);
            $manager->persist($city);
            $this->addReference('city_' . $c[1], $city);

            foreach ($c[4] as $linkedCity) {
                $lCity = new City();
                $lCity->setName($linkedCity[0]);
                $lCity->setSlug($linkedCity[1]);
                $lCity->setSynonyms($linkedCity[2]);
                $lCity->setCountry($linkedCity[3]);
                $lCity->setBigCity($city);
                $manager->persist($lCity);
                $this->addReference('city_' . $linkedCity[1], $lCity);
            }
        }

        $manager->flush();
    }
}
