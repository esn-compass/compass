<?php

namespace App\DataFixtures;

use App\Entity\Institution;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class InstitutionFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $institutions = [
            ['Institut Mines-Télécom Nord Europe', 'institut-mines-telecom-nord-europe', 'imt-lille-douai.fr',
                'villeneuve-dascq'],
            ['Université de Lille', 'universite-de-lille', 'univ-lille.fr', 'lille'],
            ['Ecole nationale supérieure d\'architecture de Marseille', 'ensa-marseille', 'marseille.archi.fr',
                'marseille'],
            ['Universite Paris 8 Vincennes Saint-Denis', 'universite-paris-8-vincennes-saint-denis', 'univ-paris8.fr',
                'saint-denis'],
            ['Universidad Autonoma de Madrid', 'universidad-autonoma-de-madrid', 'uam.es', 'madrid'],
            ['Universidad de Sevilla', 'universidad-de-sevilla', 'us.es', 'sevilla'],
            ['Karolinska Institutet', 'karolinska-institutet', 'ki.se', 'stockholm'],
            ['Instituto Politecnico do Porto', 'instituto-politecnico-do-porto', 'ipp.pt', 'porto'],
        ];

        foreach ($institutions as $i) {
            $institution = new Institution();
            $institution->setName($i[0]);
            $institution->setSlug($i[1]);
            $institution->setExternalId($i[2]);
            $institution->setCity($this->getReference('city_' . $i[3]));
            $manager->persist($institution);
            $this->addReference('institution_' . $i[1], $institution);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            CityFixtures::class,
        ];
    }
}
