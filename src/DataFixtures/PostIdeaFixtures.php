<?php

namespace App\DataFixtures;

use App\Entity\PostIdea;
use App\Enum\PostIdeaTypeEnum;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class PostIdeaFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $postIdeas = [
            PostIdeaTypeEnum::INSTITUTION => [
                [
                    'My free time in %name%',
                    'How did you manage your freetime in %name%?',
                    ['leisure']
                ],
                [
                    'The best moments of my weekends in %name%',
                    'What did you use to do during weekends in %name%?',
                    ['leisure']
                ],
                [
                    'Help to disabled students',
                    'How can disabled students can find help for their specific needs'
                    . ' (accommodation, transportation etc...) in %name%?',
                    ['accessibility']
                ],
                [
                    'The university system in %name%',
                    'Did you enjoy your studies? Have you noted any differences in the way of teaching in %name%?',
                    ['studentlife']
                ],
                [
                    'My engagement with...',
                    'Were you involved in any volunteerings or student association in %name%?',
                    ['studentlife']
                ],
                [
                    'My favorite activities in %name%',
                    'Did you feel integrated in %name%?',
                    ['studentlife']
                ],
                [
                    'The issues I faced',
                    'Did you encounter any issues in the administrative procedures in %name%?',
                    ['admin']
                ],
                [
                    'Find a local buddy to help you out!',
                    'Did any local students help you out with your administrative procedures in %name%?',
                    ['admin']
                ],
                [
                    'Find a local buddy to help you out!',
                    'Did you help international students in %name%?',
                    ['admin']
                ],
            ],
            PostIdeaTypeEnum::CITY => [
                [
                    'You have to try ...',
                    'What is/are the best restaurant(s) in %name%?',
                    ['food']
                ],
                [
                    'You have to taste ...! ',
                    'What is your favorite typical dish in %name%?',
                    ['food']
                ],
                [
                    'My supermarket in %name%',
                    'Which supermarkets in %name% were the less expensive?',
                    ['food']
                ],
                [
                    'My best place in %name%',
                    'What was your favorite places to go out in %name%?',
                    ['goingout']
                ],
                [
                    'The vibrant neighbourhood of ...',
                    'Which neighbourhoods of %name% were the most vibrant?',
                    ['goingout']
                ],
                [
                    'Spend a day in %name%',
                    'What was the most remarkable cultural or historial spot of %name% for you?',
                    ['culture']
                ],
                [
                    'Don\'t miss out benefits for students in %name%',
                    'Are there student discounts for theatres, cinemas and cultural places in %name%?',
                    ['culture']
                ],
                [
                    '%name%, an accessible city ',
                    'Did you find %name% accessible?',
                    ['accessibility']
                ],
                [
                    'Advice to easily move around %name%',
                    'Were you on exchange with people with disabilities?'
                    . ' How easy was it for them to move around %name%?',
                    ['accessibility']
                ],
                [
                    'Take you umbrella / Don\'t forget your sun cream',
                    'How was the weather like in %name%?',
                    ['weather']
                ],
                [
                    'The best bank to open an account as a student ',
                    'How easy was it for you to open a bank account in %name%?',
                    ['finance']
                ],
                [
                    'My experience in a shared flat',
                    'How did you find local accommodation in %name%?',
                    ['accommodation']
                ],
                [
                    'Look for housing in %name%',
                    'Which neighbourhoods are the most suitable to live in %name%?',
                    ['accommodation']
                ],
                [
                    'Moving around in %name%',
                    'What transports did you use the most during your exchange in %name%?',
                    ['transportation']
                ],
                [
                    'How to get faster to university',
                    'Was the transport network in %name% well served?',
                    ['transportation']
                ],
                [
                    'My new carsharing friends',
                    'Is it usual to do carsharing in %name%?',
                    ['transportation']
                ],
                [
                    'Don\'t miss out benefits for students ',
                    'Are there any discounts or travel passes for students in %name%?',
                    ['transportation']
                ],
            ],
            PostIdeaTypeEnum::COUNTRY => [
                [
                    'I discovered ...',
                    'Have you travelled around %name%?',
                    ['leisure']
                ],
                [
                    'Discovering a new hobby',
                    'Did you discover new hobbies during you mobility in %name%?',
                    ['leisure']
                ],
                [
                    'Visit ... to dance salsa avec local people!',
                    'What are the typical music and dance in %name%.?',
                    ['culture']
                ],
                [
                    'The monthly expenses on exchange in %name%',
                    'What was your monthly budget in %name% and how did you manage it?',
                    ['finance']
                ],
                [
                    'The average cost of bread is ... in %name%',
                    'Was the cost of living in %name% accessible for you?',
                    ['finance']
                ],
            ]
        ];

        foreach ($postIdeas as $category => $ideas) {
            foreach ($ideas as $idea) {
                $postIdea = new PostIdea();
                $postIdea->setType($category);
                $postIdea->setTitle($idea[0]);
                $postIdea->setQuestion($idea[1]);
                foreach ($idea[2] as $tag) {
                    $postIdea->addTag($this->getReference('tag_' . $tag));
                }
                $manager->persist($postIdea);
            }
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            CityFixtures::class,
            TagFixtures::class,
        ];
    }
}
