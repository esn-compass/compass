<?php

namespace App\DataFixtures;

use App\Entity\Tag;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class TagFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $tags = [
            ['leisure', ['freetime', 'entertainment', 'weekend', 'activities', 'sport', 'hobbies', 'travel']],
            ['goingout', ['bars', 'clubs']],
            ['culture', ['museums', 'monuments', 'librairies', 'concerts', 'theatres', 'cinema', 'arts', 'dance',
                'games', 'history', 'folklore', 'music']],
            ['food', ['gastronomy', 'restaurants', 'markets', 'supermarkets']],
            ['accessibility', ['disability', 'handicap', 'wheelchair', 'help', 'assistance']],
            ['weather', ['climate']],
            ['finance', ['money', 'cost', 'bank', 'currency', 'scholarship', 'salary', 'exchange', 'budget',
                'expenses']],
            ['accommodation', ['housing', 'apartment', 'flat', 'studentresidence', 'dorm', 'hostfamily', 'flatmates',
                'sharing', 'rent', 'contract', 'neighbourhood']],
            ['studentlife', ['campuslife', 'studies', 'volunteering', 'libraries', 'integration']],
            ['transportation', ['publictransport', 'bus', 'train', 'plane', 'subway', 'timetable', 'underground',
                'bike', 'car', 'carsharing', 'scooter']],
            ['admin', ['administrative', 'papers', 'visa', 'registration', 'enrolment']],
        ];

        foreach ($tags as $t) {
            $tag = new Tag();
            $tag->setName($t[0]);
            $tag->setSynonyms($t[1]);
            $manager->persist($tag);
            $this->addReference('tag_' . $t[0], $tag);
        }

        $manager->flush();
    }
}
