<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\UserEducation;
use App\Enum\SituationEnum;
use App\Enum\UserRoleEnum;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    private UserPasswordHasherInterface $hasher;

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager): void
    {
        // main admin account
        $user = $this->createFakeUser([
            'email' => 'admin@admin.com',
            'firstname' => 'Admin',
            'lastname' => 'Admin'
        ]);
        $user->setRoles([UserRoleEnum::ROLE_ADMIN]);
        $manager->persist($user);

        // add some users with possible information
        foreach ($this->getCredibleUsersInfo() as $u) {
            $user = $this->createFakeUser([
                'email' => $u[2],
                'firstname' => $u[0],
                'lastname' => $u[1],
                'gender' => $u[3],
                'homeCountries' => $u[4],
                'languages' => $u[5]
            ]);
            foreach ($u[6] as $s) {
                if (count($s) === 2) {
                    $user->addEducation($this->createFakeEducation([
                        'institution' => $s[0],
                        'local' => $s[1],
                    ]));
                } else {
                    $user->addEducation($this->createFakeEducation([
                        'country' => $s[0],
                        'location' => $s[1],
                        'name' => $s[2],
                        'local' => $s[3],
                    ]));
                }
            }
            $manager->persist($user);
            $this->addReference('user_' . $u[1], $user);
        }

//        // add some random users
//        for ($i = 0; $i < 10; ++$i) {
//            $user = $this->createFakeUser([]);
//            $manager->persist($user);
//        }

        $manager->flush();
    }

    private function createFakeUser(array $presetInfo) : User
    {
        $faker = Factory::create('fr_FR');
        $user = new User();
        $user
            ->setEmail($presetInfo['email'] ?? $faker->email)
            ->setFirstname($presetInfo['firstname'] ?? $faker->firstName())
            ->setLastname($presetInfo['lastname'] ?? $faker->lastName())
            ->setGender($presetInfo['gender'] ?? rand(0, 2))
            ->setBirthdate(
                $presetInfo['birthdate'] ?? $faker->dateTimeBetween('-33 years', '-17 years')
            )
            ->setHomeCountries(
                $presetInfo['homeCountries'] ?? [array_rand(array_flip(['FR', 'IT', 'ES', 'DE', 'BE']))]
            )
            ->setLanguages(
                $presetInfo['languages'] ?? ['en']
            )
            ->setCurrentSituation($presetInfo['current_situation'] ?? SituationEnum::getRandomOption())
            ->setIsVerified(1)
        ;
        $user->setPassword($this->hasher->hashPassword($user, $presetInfo['password'] ?? 'password'));
        return $user;
    }

    private function createFakeEducation(array $presetInfo) : UserEducation
    {
        $faker = Factory::create();
        $education = new UserEducation();

        if (isset($presetInfo['institution'])) {
            $education->setInstitution($this->getReference('institution_'.$presetInfo['institution']));
        } else {
            $education->setCountry($presetInfo['country'] ?? $faker->countryCode);
            $education->setLocation($presetInfo['location'] ?? $faker->city);
            $education->setName($presetInfo['name'] ?? 'International city formation center');
        }

        $education->setStartDate($presetInfo['start'] ?? '2020-09');
        $education->setEndDate($presetInfo['end'] ?? '2020-12');
        $education->setLocal($presetInfo['local'] ?? false);
        return $education;
    }

    private function getCredibleUsersInfo() : array
    {
        return [
            ['Léonie', 'Humbert', 'l.humbert@mail.fr', User::GENDER_FEMALE, ['FR'], ['en', 'fr', 'tr'], [
                ['institut-mines-telecom-nord-europe', true],
                ['TR', 'Istanbul', 'İstanbul University', false]
            ]],
            ['Paul', 'Dupont', 'pdupont@mail.fr', User::GENDER_MALE, ['FR'], ['en', 'fr', 'ja'], [
                ['universite-paris-8-vincennes-saint-denis', true],
                ['JP', 'Kyoto', 'Doshisha University', false]
            ]],
            ['Isabelle', 'Torres', 'isatorres@mail.com', User::GENDER_FEMALE, ['FR', 'ES'], ['en', 'es'], [
                ['universidad-autonoma-de-madrid', true],
                ['ensa-marseille', false]
            ]],
            ['Hans', 'Schwartz', 'hans.schwartz@mail.fr', User::GENDER_MALE, ['DE'], ['en', 'de'], [
                ['DE', 'Hamburg', 'Hamburg University of Technology', true],
                ['karolinska-institutet', false]
            ]],
            ['Steven', 'Oliveira', 'soli@mail.pt', User::GENDER_MALE, ['PT'], ['en', 'pt', 'es'], [
                ['instituto-politecnico-do-porto', true],
                ['universidad-de-sevilla', false]
            ]]
        ];
    }

    public function getDependencies(): array
    {
        return [
            InstitutionFixtures::class,
        ];
    }
}
