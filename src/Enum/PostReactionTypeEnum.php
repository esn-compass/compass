<?php

declare(strict_types=1);

namespace App\Enum;

class PostReactionTypeEnum extends AbstractEnum
{
    public const LIKE = 'like';
    public const LOVE = 'love';
    public const USEFUL = 'useful';

    protected static array $choices = [
        self::LIKE => 'Like reaction',
        self::LOVE => 'Love reaction',
        self::USEFUL => 'Useful reaction',
    ];
}
