<?php

declare(strict_types=1);

namespace App\Enum;

class UserStatusEnum extends AbstractEnum
{
    public const SAVED = 'saved';

    protected static array $choices = [
        self::SAVED => 'Saved in database',
    ];
}
