<?php

declare(strict_types=1);

namespace App\Enum;

class OpenCountryEnum extends AbstractEnum
{
    public const AUSTRIA = 'AT';
    public const BELGIUM = 'BE';
    public const BULGARIA = 'BG';
    public const SWITZERLAND = 'CH';
    public const CYPRUS = 'CY';
    public const CZECH_REPUBLIC = 'CZ';
    public const GERMANY = 'DE';
    public const DENMARK = 'DK';
    public const ESTONIA = 'EE';
    public const SPAIN = 'ES';
    public const FINLAND = 'FI';
    public const FRANCE = 'FR';
    public const UK = 'GB';
    public const GREECE = 'GR';
    public const CROATIA = 'HR';
    public const HUNGARY = 'HU';
    public const IRELAND = 'IE';
    public const ICELAND = 'IS';
    public const ITALY = 'IT';
    public const LIECHTENSTEIN = 'LI';
    public const LITHUANIA = 'LT';
    public const LUXEMBOURG = 'LU';
    public const LATVIA = 'LV';
    public const MALTA = 'MT';
    public const NETHERLANDS = 'NL';
    public const NORWAY = 'NO';
    public const POLAND = 'PL';
    public const PORTUGAL = 'PT';
    public const ROMANIA = 'RO';
    public const SERBIA = 'RS';
    public const SWEDEN = 'SE';
    public const SLOVENIA = 'SI';
    public const SLOVAKIA = 'SK';
    public const TURKEY = 'TR';

    protected static array $choices = [
        self::AUSTRIA => 'Austria',
        self::BELGIUM => 'Belgium',
        self::BULGARIA => 'Bulgaria',
        self::SWITZERLAND => 'Switzerland',
        self::CYPRUS => 'Cyprus',
        self::CZECH_REPUBLIC => 'Czech Republic',
        self::GERMANY => 'Germany',
        self::DENMARK => 'Denmark',
        self::ESTONIA => 'Estonia',
        self::SPAIN => 'Spain',
        self::FINLAND => 'Finland',
        self::FRANCE => 'France',
        self::UK => 'United Kingdom',
        self::GREECE => 'Greece',
        self::CROATIA => 'Croatia',
        self::HUNGARY => 'Hungary',
        self::IRELAND => 'Ireland',
        self::ICELAND => 'Iceland',
        self::ITALY => 'Italy',
        self::LIECHTENSTEIN => 'Liechtenstein',
        self::LITHUANIA => 'Lithuania',
        self::LUXEMBOURG => 'Luxembourg',
        self::LATVIA => 'Latvia',
        self::MALTA => 'Malta',
        self::NETHERLANDS => 'Netherlands',
        self::NORWAY => 'Norway',
        self::POLAND => 'Poland',
        self::PORTUGAL => 'Portugal',
        self::ROMANIA => 'Romania',
        self::SERBIA => 'Serbia',
        self::SWEDEN => 'Sweden',
        self::SLOVENIA => 'Slovenia',
        self::SLOVAKIA => 'Slovakia',
        self::TURKEY => 'Turkey',
    ];

    /**
     * Return countries that match search
     * @param array $search
     * @return array<string>
     */
    public static function searchCountries(array $search): array
    {
        $countries = [];
        foreach ($search as $searchPart) {
            foreach (self::$choices as $iso => $name) {
                if (false !== strpos(strtolower($name), strtolower($searchPart))) {
                    $countries[] = $iso;
                }
            }
        }
        return $countries;
    }
}
