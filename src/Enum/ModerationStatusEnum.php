<?php

declare(strict_types=1);

namespace App\Enum;

class ModerationStatusEnum extends AbstractEnum
{
    public const WAITING = 'waiting';
    public const ACCEPTED = 'accepted';
    public const REFUSED = 'refused';

    protected static array $choices = [
        self::WAITING => 'Waiting for moderation',
        self::ACCEPTED => 'Accepted by moderator',
        self::REFUSED => 'Refused by moderator',
    ];
}
