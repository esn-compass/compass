<?php

declare(strict_types=1);

namespace App\Enum;

class SnsVisibilityEnum extends AbstractEnum
{
    public const PRIVATE = 'private';
    public const OPEN_TO_REQUEST = 'open_to_request';
    public const PUBLIC = 'public';

    protected static array $choices = [
        self::PRIVATE => 'Private',
        self::OPEN_TO_REQUEST => 'Open to request',
        self::PUBLIC => 'Public'
    ];
}
