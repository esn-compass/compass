<?php

declare(strict_types=1);

namespace App\Enum;

class UserRoleEnum extends AbstractEnum
{
    public const ROLE_USER = 'ROLE_USER';
    public const ROLE_ADMIN = 'ROLE_ADMIN';

    protected static array $choices = [
        self::ROLE_USER => 'User',
        self::ROLE_ADMIN => 'Admin',
    ];
}
