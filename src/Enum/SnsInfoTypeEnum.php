<?php

declare(strict_types=1);

namespace App\Enum;

class SnsInfoTypeEnum extends AbstractEnum
{
    public const INSTAGRAM = 'instagram';
    public const FACEBOOK = 'facebook';
    public const TWITTER = 'twitter';
    public const TIKTOK = 'tiktok';

    protected static array $choices = [
        self::INSTAGRAM => 'https://www.instagram.com/',
        self::FACEBOOK => 'https://www.facebook.com/',
        self::TWITTER => 'https://twitter.com/',
        self::TIKTOK => 'https://www.tiktok.com/@'
    ];
}
