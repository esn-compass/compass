<?php

declare(strict_types=1);

namespace App\Enum;

class PostTypeEnum extends AbstractEnum
{
    public const POST = 'post';

    protected static array $choices = [
        self::POST => 'Post',
    ];
}
