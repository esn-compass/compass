<?php

namespace App\Enum;

use phpDocumentor\Reflection\Types\Boolean;

abstract class AbstractEnum
{
    protected static array $choices = [];

    public static function getOptions() : array
    {
        return static::$choices;
    }

    public static function getReadableChoices() : array
    {
        return array_flip(static::$choices);
    }

    public static function getLabel(?string $value) : string
    {
        if (false === isset(static::$choices[$value])) {
            return '';
        }
        return static::$choices[$value];
    }

    public static function getRandomOption() : string
    {
        return array_rand(static::$choices);
    }

    public static function isValidValue(string $value) : bool
    {
        return in_array($value, array_keys(static::$choices));
    }
}
