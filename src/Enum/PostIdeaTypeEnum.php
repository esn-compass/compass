<?php

declare(strict_types=1);

namespace App\Enum;

class PostIdeaTypeEnum extends AbstractEnum
{
    public const INSTITUTION = 'institution';
    public const CITY = 'city';
    public const COUNTRY = 'country';

    protected static array $choices = [
        self::INSTITUTION => 'Institution related ideas',
        self::CITY => 'City related ideas',
        self::COUNTRY => 'Country related ideas',
    ];
}
