<?php

declare(strict_types=1);

namespace App\Enum;

class SituationEnum extends AbstractEnum
{
    public const LOCAL = 'local';
    public const MOBILITY = 'mobility';

    protected static array $choices = [
        self::LOCAL => 'Local student',
        self::MOBILITY => 'Mobility',
    ];
}
